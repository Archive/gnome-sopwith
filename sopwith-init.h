/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */


#ifndef SOPWITH_INIT_H
#define SOPWITH_INIT_H

#include "sw.h"
#include "sopwith-object.h"

void           sopwith_init         (void);
void           sopwith_restart      (void);

SopwithObject *create_plane         (SopwithObject *obp);

void           create_enemy_plane   (SopwithObject *obp);
void           create_player_plane  (SopwithObject *obp);
void           create_bird          (SopwithObject *obop,
				     int            i);
void           fire_shot            (SopwithObject *obo, 
				     SopwithObject *target);
void           drop_bomb            (SopwithObject *obop);
void           fire_missile         (SopwithObject *obop);
void           fire_starburst       (SopwithObject *obop);
void           create_explosion     (SopwithObject *obop,
				     int            small);
void           create_smoke         (SopwithObject *obop);



void           draw_lives_gauge     (SopwithObject *ob);
void           draw_fuel_gauge      (SopwithObject *ob);


void           draw_bomb_gauge      (SopwithObject *ob);
void           draw_shot_gauge      (SopwithObject *ob);
void           draw_missile_gauge   (SopwithObject *ob);
void           draw_starburst_gauge (SopwithObject *ob);


void           set_velocity         (SopwithObject *obj,
				     int            xv,
				     int            yv);

void           initialize_display   (gboolean       reset);

#endif /* SOPWITH_INIT_H */

