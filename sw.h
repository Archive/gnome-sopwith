/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
        sw.h     -      SW  Manifest Definitions

                        Copyright (C) 1984-2000 David L. Clark.

                        All rights reserved except as specified in the
                        file license.txt.  Distribution of this file
                        without the license.txt file accompanying is
                        prohibited.

                        Author: Dave Clark

        Modification History:
                        84-02-02        Development
                        84-06-12        PCjr Speed-up
                        85-04-02        Asynch Mode
                        85-10-31        Atari
                        87-03-09        Microsoft compiler
                        87-03-12        Wounded airplanes
                        87-03-12        Crashed planes stay longer at home.
                        87-03-13        Splatted bird symbol
                        87-03-30        Novice Player
                        87-03-31        Missiles
                        87-04-04        Missile and starburst support
                        87-04-08        Delay between starbursts
*/

#ifndef SW_H
#define SW_H

#include <glib.h>


/*  Constants  */

#define MAX_X           3000            /*  Maximum X coordinate            */
#define MAX_Y           200             /*  Maximum Y coordinate            */
#define MIN_SPEED       4               /*  Minimum plane speed             */
#define MAX_SPEED       8               /*  Maximum plane speed             */
#define MAX_THROTTLE    4               /*  Maximum acceleration            */

#define MAXCRCOUNT      10              /*  Number of turns as crashed      */
#define FALLCOUNT       10              /*  Moves between falling plane adj */
#define STALLCOUNT      6               /*  Moves between stalling plane adj*/
#define TARGHITCOUNT    10              /*  Target hit count before explod'n*/

#define SCR_WDTH        320             /*  Screen Width                    */
#define SCR_HGHT        200             /*  Screen Height                   */
#define SCR_CENTR       152             /*  Centre column of screen         */
#define SCR_SEGM        0xB800          /*  Screen Video segment            */
#define SCR_ROFF        0x2000          /*  Screen odd rastor line offset   */
#define SCR_LINW        80              /*  Screen line width in bytes      */
#define SCR_MNSH        16              /*  Minumum line number to shift    */
#define SCR_MXSH        75              /*  Maximum line number to shift    */
#define SCR_LIMIT       180             /*  Screen shift limits             */

#define WRLD_RSX        (MAX_X / SCR_WDTH * 2 + 1) /*  World display X and  */
#define WRLD_RSY        (MAX_Y / SCR_MNSH + 1)     /*  Y divisors           */

#define MINFLCKX        ( 0     + SCR_WDTH + 50 )  /*  Bird flock travel    */
#define MAXFLCKX        ( MAX_X - SCR_WDTH - 50 )  /*    limits             */



#define HITSYMS         2               /*  Number of hit symbols per plane */
#define ANGLES          16              /*  Number of angle increments      */
#define ORIENTS         2               /*  Number of plane orientations    */
#define SYMBYTES        64              /*  Bytes in a symbol               */
#define WINSIZES        4               /*  Number of winner plane sizes    */
#define WINBYTES        64              /*  Bytes in a winner symbol        */
#define SYM_WDTH        16              /*  Symbol width in pixels          */
#define SYM_HGHT        16              /*  Symbol height in pixels         */
#define BOMBBYTES       16              /*  Bytes in a bomb symbol          */
#define BOMBANGS        8               /*  Number of bomb angles           */
#define TARGBYTES       64              /*  Bytes in a target symbol        */
#define TARGORIENTS     4               /*  Number of target types          */
#define EXPLSYMS        8               /*  Number of explosion symbols     */
#define EXPBYTES        16              /*  Bytes in an explosion symbol    */
#define FLCKSYMS        2               /*  Number of flock symbols         */
#define FLKBYTES        64              /*  Bytes in a flock symbol         */
#define BIRDSYMS        2               /*  Number of bird symbols          */
#define BRDBYTES        2               /*  Bytes in a bird symbol          */
#define OXSYMS          2               /*  Number of ox symbols            */
#define OXBYTES         64              /*  Bytes in an ox symbol           */
#define GHSTBYTES       16              /*  Bytes in a ghost symbol         */
#define SHOTBYTES       64              /*  Bytes in a shot window symbol   */
#define SPLTBYTES       256             /*  Bytes in a splatted bird symbol */
#define MISCBYTES       16              /*  Bytes in a missile symbol       */
#define MISCANGS        16              /*  Number of missile angles        */
#define BRSTBYTES       16              /*  Bytes in a starburst symbol     */
#define BRSTSYMS        2               /*  Number of starburst symbols     */

typedef enum {
	SINGLE = 0,
	MULTIPLE = 1,
	COMPUTER = 2,
	ASYNCH = 3,
	NOVICE = 4
} SopwithPlayMode;

#define MAXROUNDS       200             /* Maximum number of rounds of shot */
#define MAXBOMBS        5               /* Maximum number of bombs available*/
#define MAXMISSILES     5               /* Maximum number of missiles       */
#define MAXBURSTS       5               /* Maximum nuber of starbursts      */
#define MAXFUEL         ( 3 * MAX_X )   /* Maximum Number of turns between  */
                                        /*  refuellings                     */
#define MAXCRASH        5               /* Mximum number of crashes allowed */

#define MAX_PLYR        4               /* Maximum number of players        */
#define MAX_TARG        20              /* Maximum number of targets        */
#define MAX_OBJS        100             /* Maximum number of objects        */
#define MAX_FLCK        4               /* Maximum number of flocks         */
#define MAX_BIRD        1               /* Maximum number of stray birds    */
                                        /*    per flock                     */
#define MAX_GAME        7               /* Maximum number of games          */
#define MAX_OXEN        2               /* Maximum number of oxen           */

#define BULSPEED        10              /* Bullet speed    */
#define BULLIFE         10              /* Bullet life     */
#define BOMBLIFE        5               /* Moves between bomb course adj   */
#define MISSLIFE        50              /* Missile life                    */
#define BURSTLIFE       20              /* Starburst life                  */
#define EXPLLIFE        3               /* Moves between explosion "  "    */
#define SMOKELIFE       10              /* Smoke life                      */
#define BIRDLIFE        4               /* Moves between bird flaps        */
#define FLOCKLIFE       5               /* Moves between flop flaps        */


#define WINNER          1               /*  End of game status             */
#define LOSER           2

#define NEAR            ( 150 * 150 )   /* Computer control distances        */
#define NEARAPPROACH    200
#define CLOSE           32
#define DEADON          ( 4 * 4 )
#define HOME            16
#define SAFERESET       32

#define QUIT            -5000           /* Plane life value when quitting    */

#define S_TITLE         05
#define S_EXPLOSION     10              /*  Sound priorities                */
#define S_BOMB          20
#define S_SHOT          30
#define S_FALLING       40
#define S_HIT           50
#define S_PLANE         60

#define K_ACCEL         0x0001          /* Keyboard word masks               */
#define K_DEACC         0x0002
#define K_FLAPU         0x0004
#define K_FLAPD         0x0008
#define K_FLIP          0x0010
#define K_SHOT          0x0020
#define K_BOMB          0x0100
#define K_HOME          0x0200
#define K_SOUND         0x0400
#define K_BREAK         0x0800
#define K_MISSILE       0x1000
#define K_STARBURST     0x2000

#define K_ASYNACK       0x40


#define COMM_FILE       "c:sopwith?.dta" /*  Multi user communications file  */
#define COMM_CMD        "c:semaphor\0   "/*  Multi-user semaphor file        */



typedef struct tt {                     /*  Continuous tone table entry    */
        unsigned  tt_tone;
        unsigned  tt_chng;
        struct tt *tt_next;
        struct tt *tt_prev;
}       TONETAB;

typedef struct SopwithGameParameters SopwithGameParameters;

struct SopwithGameParameters {                                /*  Game structure          */
        int        gm_x[MAX_PLYR*2];
        int        gm_orient[MAX_PLYR*2];
        unsigned (*gm_randf) ();
        unsigned   gm_rseed;
        int      (*gm_specf) ();
        int        gm_xtarg[MAX_TARG];
        int        gm_ttarg[MAX_TARG];
};



typedef struct {                                /*  Communications buffer   */
        unsigned mu_maxplyr;
        unsigned mu_numplyr;
        unsigned mu_lstplyr;
        unsigned mu_key[MAX_PLYR];
        unsigned mu_state[MAX_PLYR];
        unsigned mu_explseed;
}       MULTIO;

typedef struct {                                /*  Output message record   */
        char     msgo_cmd;
        char     msgo_port;
        char     msgo_ssgnl;
        char     msgo_rsgnl;
        char     msgo_1fill[6];
        MULTIO   msgo_buff;
        char     msgo_2fill[1];
}       MSGOUT;

typedef struct {                                /*  Input message record    */
        char     msgi_port;
        char     msgi_1fill[2];
        char     msgi_myport;
        char     msgi_2fill[6];
        MULTIO   msgi_buff;
        char     msgi_3fill[1];
}       MSGIN;



typedef struct {                        /*  Old display parameters for    */
        int     ow_xorplot;             /*  each object                   */
        int     ow_x, ow_y;
}       OLDWDISP;


#define COS(x)  sintab[(x+(ANGLES/4))%ANGLES]
#define SIN(x)  sintab[x%ANGLES]

typedef int BIOFD;

typedef guchar GRNDTYPE;

#endif
