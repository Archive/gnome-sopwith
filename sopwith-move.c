/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
        swmove   -      SW move all SopwithObject and players

                        Copyright (C) 1984-2000 David L. Clark.

                        All rights reserved except as specified in the
                        file license.txt.  Distribution of this file
                        without the license.txt file accompanying is
                        prohibited.

                        Author: Dave Clark

        Modification History:
                        84-02-07        Development
                        85-10-31        Atari
                        87-03-09        Microsoft compiler.
                        87-03-12        Wounded airplanes.
                        87-03-12        Crashed planes stay longer at home.
                        87-03-12        Proper ASYCHRONOUS end of game.
                        87-03-12        Prioritize bombs/shots over flaps.
                        87-03-12        Computer plane heads home at end.
                        87-03-30        Novice Player
                        87-03-31        Allow wounded plane to fly home
                        87-04-01        Missiles.
                        87-04-04        Missile and starburst support.
                        87-04-09        Delay between starbursts.
			2000-12-29      Started GNOME porting. See
                                          ChangeLog for further changes.  
*/

#include <config.h>
#include "sopwith-move.h"

#include "sopwith-auto-pilot.h"
#include "sopwith-collision.h"
#include "sopwith-draw.h"
#include "sopwith-graphics.h"
#include "sopwith-init.h"
#include "sopwith-end.h"
#include "sopwith-object.h"
#include "sw.h"

#include <stdio.h>
#include <stdlib.h>

extern  int      displx, disprx;         /*  Display left and right bounds */
extern  int      dispdx;                 /*  Display shift                 */
extern  SopwithObject *objtop,                 /*  Start of object list.         */
                *objfree,                /*  Free object list.             */
                *deltop, *delbot;        /*  Newly deallocated SopwithObject     */
extern  SopwithObject  oobjects[];       /* Original plane object description*/
extern  SopwithObject *compnear[];       /*  Array of planes near computers*/
extern  int      lcompter[];             /* Computer plane territory       */
extern  int      rcompter[];             /* Computer plane territory       */
extern  int      playmode;               /*  Play mode                     */
extern  MULTIO  *multbuff;               /*  Communications buffer         */
extern  int      sintab[];               /*  sine table based on angles    */
extern  GRNDTYPE ground[];               /*  Ground height by pixel        */
extern  int      keydelay;               /*  Number of displays per keystr */
extern  int      multkey;                /* Keystroke to be passed         */
extern  char     swplnsym[][ANGLES][SYMBYTES];/* plane pixel array         */
extern  char     swhitsym[][SYMBYTES];   /*  Hit plane pixel array         */
extern  char     swbmbsym[][BOMBBYTES];  /* Bomb pixel array               */
extern  char     swmscsym[][MISCBYTES];  /* Missile pixel array            */
extern  char     swbstsym[][BRSTBYTES];  /* Statburst symbol array         */
extern  char     swtrgsym[][TARGBYTES];  /* Target Pixel array             */
extern  char     swwinsym[][WINBYTES];   /* Winner Pixel Array             */
extern  char     swhtrsym[];             /*  Hit target pixel array         */
extern  char     swexpsym[][EXPBYTES];   /*  Explosion pixel array          */
extern  char     swflksym[][FLKBYTES];   /*  Flock pixel array              */
extern  char     swbrdsym[][BRDBYTES];   /*  Bird pixel array               */
extern  char     swoxsym[][OXBYTES];     /*  Ox pixel array                 */
extern  int      dispcomp();
extern  int      counttick, countmove;   /* Performance counters            */
extern  gboolean compplane;              /* Moving computer plane flag      */
extern  gboolean plyrplane;              /* Moving player plane flag        */
extern  int      currobx;                /* Current object index             */
extern  int      player;
extern  int      soundflg;               /*  Sound flag                      */
extern  int      endsts[];               /* End of game status and move count*/
extern  int      endcount;
extern  gboolean goingsun;               /* Heading for the sun flag         */
extern  int      gamenum;                /* Game number                      */
extern  int      gmaxspeed,gminspeed;    /* Speed range based on game number */
extern  int      targrnge;               /* Target range based on game number*/
extern  int      dispcnt;                /* Displays to delay keyboard       */
extern  int      endstat;                /* End of game status for curr. move*/

extern  int      missok;                 /* Missiles supported               */

static  gboolean quit;


static void interpret_key           (SopwithObject *obp,
				     int      key);
static int  move_plane              (SopwithObject *obp);
static void find_SopwithObject_near_plane (SopwithObject *obp);
static void stall_plane             (SopwithObject *obp);
static int  find_symbol_angle       (SopwithObject *ob);
static void refuel                  (SopwithObject *obp);
static void adjust_falling_object   (SopwithObject *obp);


void
move_all_SopwithObject (void)
{
	SopwithObject *ob, *obn;

        if (deltop) {
                delbot->ob_next = objfree;
                objfree = deltop;
                deltop = delbot = NULL;
        }

        if (++dispcnt >= keydelay) {
                dispcnt = 0;
	}

        ob = objtop;

        while (ob) {
                obn = ob->ob_next;
                ob->ob_delflg = ob->ob_drwflg;
                ob->ob_oldsym = ob->ob_newsym;
                ob->ob_drwflg = (*ob->ob_movef) (ob);
#if 0
                if (((playmode == MULTIPLE) || (playmode == ASYNCH))
		    && (ob->ob_index == multbuff->mu_maxplyr)
		    && (!dispcnt)) {
                        if (playmode == MULTIPLE) {
                                multput ();
                        } else {
                                asynput ();
			}
		}
#endif
		ob = obn;
        }
        ++countmove;
}


int
move_player_plane (SopwithObject *obp)
{
	SopwithObject *ob;
	gboolean rc;
	int     oldx;
	
        compplane = FALSE;
        plyrplane = TRUE;
	
        ob = obp;
        currobx = ob->ob_index;

	endstat = endsts[player];
        if (endstat) {
                if (--endcount <= 0) {
                        if ((playmode != MULTIPLE)
			    && (playmode != ASYNCH)
			    && (!quit)) {
                                sopwith_restart ();
			}
                        sopwith_end (NULL, TRUE);
                }
	}
	
        if (!dispcnt) {
#if 0
		if (playmode == MULTIPLE) {
                        multkey = multget (ob);
		} else if (playmode == ASYNCH) {
                        multkey = asynget (ob);
                } else {
			/* foo */
                }
#endif
		multkey = getchar ();
#if 0 /* flush keyboard buffer */
		swflush ();
#endif
		interpret_key (ob, multkey);
	} else {
		ob->ob_flaps = 0;
                ob->ob_bfiring = ob->ob_bombing = FALSE;
                ob->ob_mfiring = NULL;
        }
	
        if (((ob->ob_state == CRASHED) || (ob->ob_state == GHOSTCRASHED))
	    && (ob->ob_hitcount <= 0)) {
                ++ob->ob_crashcnt;
                if ((endstat != WINNER)
		    && ((ob->ob_life <= QUIT)
                        || ((playmode != MULTIPLE)
			    && (playmode != ASYNCH)
			    && (ob->ob_crashcnt >= MAXCRASH)))) {
                        if (!endstat) {
                                loser (ob);
			}
                } else {
                        create_player_plane (ob);
                        initialize_display (TRUE);
                        if (endstat == WINNER) {
#if 0
                                winner (ob);
#endif
                        }
                }
        }
	
	oldx = ob->ob_x;

        rc = move_plane (ob);

        if ((oldx <= SCR_LIMIT) || (oldx >= (MAX_X - SCR_LIMIT))) {
                dispdx = 0;
	} else {
                displx += (dispdx = ob->ob_x - oldx);
                disprx += dispdx;
        }
	
        if (!ob->ob_athome) {
                use_on_screen_buffer ();
                if (ob->ob_firing) {
                        draw_shot_gauge (ob);
		}
		if (ob->ob_bombing) {
                        draw_bomb_gauge (ob);
		}
                if (ob->ob_mfiring) {
                        draw_missile_gauge (ob);
		}
                if (ob->ob_bfiring) {
                        draw_starburst_gauge (ob);
		}
        }

        return (rc);
}



static void
interpret_key (SopwithObject *obp,
	       int key)
{
#if 0 /* Needs to be rewritten */
	SopwithObject *ob;
	int     state;

        ob = obp;
        ob->ob_flaps = 0;
        ob->ob_bombing = ob->ob_bfiring = 0;
        ob->ob_mfiring = ob->ob_firing = NULL;
        if (((state = ob->ob_state) != FLYING)
	    && (state != STALLED)
	    && (state != FALLING)
	    && (state != WOUNDED)
	    && (state != WOUNDSTALL)
	    && (state != GHOST)
	    && (state != GHOSTSTALLED)) {
                return;
	}

        if (state != FALLING) {
                if (endstat) {
                        if ((endstat == LOSER) && plyrplane)
                                go_home (ob);
                        return;
                }

                if (key & K_BREAK) {
                        ob->ob_life = QUIT;
                        ob->ob_home = FALSE;
                        if (ob->ob_athome) {
                                ob->ob_state = state = ( state >= FINISHED )
                                        ? GHOSTCRASHED : CRASHED;
                                ob->ob_hitcount = 0;
                        }
                        if (plyrplane)
                                quit = TRUE;
                }

                if (key & K_HOME) {
                        if ((state == FLYING)
			    || (state == GHOST)
			    || (state == WOUNDED)) {
                                ob->ob_home = TRUE;
			}
		}
        }

        if ((countmove & 1)
	    || ((state != WOUNDED) && (state != WOUNDSTALL))) {
                if (key & K_FLAPU) {
                        ++ob->ob_flaps;
                        ob->ob_home = FALSE;
                }

                if (key & K_FLAPD) {
                        --ob->ob_flaps;
                        ob->ob_home = FALSE;
                }

                if (key & K_FLIP) {
                        ob->ob_orient = !ob->ob_orient;
                        ob->ob_home = FALSE;
                }

                if (key & K_DEACC) {
                        if (ob->ob_accel) {
                                --ob->ob_accel;
			}
                        ob->ob_home = FALSE;
                }

                if (key & K_ACCEL) {
                        if (ob->ob_accel < MAX_THROTTLE) {
                                ++ob->ob_accel;
			}
                        ob->ob_home = FALSE;
                }
        }
	
        if ((key & K_SHOT) && (state < FINISHED)) {
                ob->ob_firing = ob;
	}

        if ((key & K_MISSILE) && (state < FINISHED)) {
                ob->ob_mfiring = ob;
	}

        if ((key & K_BOMB) && (state < FINISHED)) {
                ob->ob_bombing = TRUE;
	}

        if ((key & K_STARBURST) && (state < FINISHED)) {
                ob->ob_bfiring = TRUE;
	}

        if (key & K_SOUND) {
                if (plyrplane) {
                        if (soundflg) {
                                sound (0, 0, NULL);
                                swsound ();
                        }
                        soundflg = !soundflg;
                }
	}

	if (ob->ob_home) {
                go_home (ob);
	}
#endif
}



int
move_enemy_plane (SopwithObject *obp)
{
	SopwithObject *ob;

        compplane = TRUE;
        plyrplane = FALSE;

        ob = obp;
        ob->ob_flaps = 0;
        ob->ob_bfiring = ob->ob_bombing = FALSE;
        ob->ob_mfiring = NULL;

        endstat = endsts[currobx = ob->ob_index];

        if (!dispcnt) {
                ob->ob_firing = NULL;
	}

        switch (ob->ob_state) {
                case WOUNDED:
                case WOUNDSTALL:
                        if (countmove & 1) {
                                break;
			}
                case FLYING:
                case STALLED:
                        if (endstat) {
                                go_home( ob );
                                break;
                        }
                        if (!dispcnt) {
                                auto_pilot (ob);
			}
                        break;

                case CRASHED:
                        ob->ob_firing = NULL;
                        if ((ob->ob_hitcount <= 0) && (!endstat))
                                create_enemy_plane (ob);
                        break;

                default:
                        ob->ob_firing = NULL;
                        break;
        }

        return move_plane (ob);
}


static char gravity[] = {0,-1,-2,-3,-4,-3,-2,-1,
			 0, 1, 2, 3, 4, 3, 2, 1 };

static int
move_plane (SopwithObject *obp)
{
	SopwithObject *ob;
	int nangle, nspeed, state, limit, update;
	int x, y, newstate, stalled;

        ob = obp;

        switch (state = ob->ob_state) {
                case FINISHED:
                case WAITING:
                        return FALSE;
                case CRASHED:
                case GHOSTCRASHED:
                        --ob->ob_hitcount;
                        break;
                case FALLING:
                        ob->ob_hitcount -= 2;
                        if ((ob->ob_dy < 0) && ob->ob_dx) {
                                if (ob->ob_orient ^ (ob->ob_dx < 0)) {
                                        ob->ob_hitcount -= ob->ob_flaps;
                                } else {
                                        ob->ob_hitcount += ob->ob_flaps;
				}
			}
                        if (ob->ob_hitcount <= 0) {
                                if (ob->ob_dy < 0) {
                                        if (ob->ob_dx < 0) {
                                                ++ob->ob_dx;
                                        } else if (ob->ob_dx > 0) {
                                                --ob->ob_dx;
                                        } else {
                                                ob->ob_orient = !ob->ob_orient;
					}
				}

                                if (ob->ob_dy > -10) {
                                        --ob->ob_dy;
				}
                                ob->ob_hitcount = FALLCOUNT;
                        }
                        ob->ob_angle = find_symbol_angle (ob) << 1;
                        if (ob->ob_dy <= 0) {
#if 0
                                initsound (ob, S_FALLING);
#endif
			}
                        break;

                case STALLED:
                        newstate = FLYING;
                        goto commonstall;

                case GHOSTSTALLED:
                        newstate = GHOST;
                        goto commonstall;

                case WOUNDSTALL:
                        newstate = WOUNDED;

                commonstall:
                        if (!(stalled = (ob->ob_angle != (3 * ANGLES / 4))
			      || (ob->ob_speed < gminspeed))) {
                                ob->ob_state = state = newstate;
			}
                        goto controlled;

                case FLYING:
                case WOUNDED:
                case GHOST:
			stalled = (ob->ob_y >= MAX_Y);
                        if (stalled) {
                                if (playmode == NOVICE) {
                                        ob->ob_angle = (3 * ANGLES / 4);
                                        stalled = FALSE;
                                } else {
                                        stall_plane (ob);
                                        state = ob->ob_state;
                                }
                        }

                controlled:
                        if (goingsun && plyrplane) {
                                break;
			}
			
                        if ((ob->ob_life <= 0) && !ob->ob_athome
			    && ((state == FLYING)
				|| (state == STALLED)
				|| (state == WOUNDED)
				|| (state == WOUNDSTALL))) {
                                hit_plane (ob);
                                score_for_plane (ob);
                                return move_plane (ob);
			}
			
			if (ob->ob_firing) {
                                fire_shot (ob, NULL);
			}
			
                        if (ob->ob_bombing) {
                                drop_bomb (ob);
			}
			
                        if (ob->ob_mfiring) {
                                fire_missile (ob);
			}
			
                        if (ob->ob_bfiring) {
                                fire_starburst (ob);
			}
			
                        nangle = ob->ob_angle;
                        nspeed = ob->ob_speed;
			
			update = ob->ob_flaps;
                        if (update) {
                                if (ob->ob_orient) {
                                        nangle -= update;
                                } else {
                                        nangle += update;
				}
                                nangle = (nangle + ANGLES) % ANGLES;
                        }
			
                        if (!(countmove & 0x0003)) {
                                if ((!stalled) && (nspeed < gminspeed)
				    && (playmode != NOVICE)) {
                                        --nspeed;
                                        update = TRUE;
                                } else {
                                        limit = gminspeed
                                                + ob->ob_accel
                                                + gravity[nangle];
                                        if (nspeed < limit) {
                                                ++nspeed;
                                                update = TRUE;
                                        } else if (nspeed > limit) {
                                                --nspeed;
                                                update = TRUE;
                                        }
                                }
                        }
			
                        if (update) {
                                if (ob->ob_athome) {
                                        if (ob->ob_accel || ob->ob_flaps) {
                                                nspeed = gminspeed;
                                        } else {
                                                nspeed = 0;
					}
                                } else if ((nspeed <= 0) && !stalled) {
                                        if (playmode == NOVICE) {
                                                nspeed = 1;
                                        } else {
                                                stall_plane (ob);
                                                return move_plane (ob);
                                        }
                                }
				
                                ob->ob_speed = nspeed;
                                ob->ob_angle = nangle;
				
                                if (stalled) {
                                        ob->ob_dx = ob->ob_ldx = ob->ob_ldy = 0;
                                        ob->ob_dy = -nspeed;
                                } else {
                                        set_velocity (ob,
						      nspeed * COS (nangle),
						      nspeed * SIN (nangle));
				}
                        }
			
                        if (stalled) {
                                if (!--ob->ob_hitcount) {
                                        ob->ob_orient = !ob->ob_orient;
                                        ob->ob_angle = ((3 * ANGLES / 2)
							- ob->ob_angle)
						% ANGLES;
                                        ob->ob_hitcount = STALLCOUNT;
                                }
                        }
			
                        if (!compplane) {
                                if (plyrplane
				    && (ob->ob_speed >
                                        (ob->ob_life % (MAXFUEL/10)))) {
                                        use_on_screen_buffer ();
                                        draw_fuel_gauge (ob);
                                }
                                ob->ob_life -= ob->ob_speed;
                        }
			
                        if (ob->ob_speed) {
                                ob->ob_athome = FALSE;
			}
                        break;
        }
	
        if ((endstat == WINNER) && plyrplane && goingsun) {
                ob->ob_newsym = swwinsym[endcount / 18];
        } else { 
                ob->ob_newsym = (ob->ob_state == FINISHED)
			? NULL
			: (((ob->ob_state == FALLING)
			    && (!ob->ob_dx) && (ob->ob_dy < 0))
			   ? swhitsym[ob->ob_orient]
			   : swplnsym[ob->ob_orient][ob->ob_angle]);
	}
	
        move_object (ob, &x, &y);
	
        if (x < 0) {
                x = ob->ob_x = 0;
        } else {
                if (x >= (MAX_X - 16)) {
                        x = ob->ob_x = MAX_X - 16;
		}
	}
	
        if ((!compplane)
	    && ((ob->ob_state == FLYING)
		|| (ob->ob_state == STALLED)
		|| (ob->ob_state == WOUNDED)
		|| (ob->ob_state == WOUNDSTALL))
	    && !endsts[player]) {
                find_SopwithObject_near_plane (ob);
	}

        delete_x (ob);
        insert_x (ob, ob->ob_xnext);

        if (ob->ob_bdelay) {
                --ob->ob_bdelay;
	}
        if (ob->ob_mdelay) {
                --ob->ob_mdelay;
	}
        if (ob->ob_bsdelay) {
                --ob->ob_bsdelay;
	}

        if ((!compplane) && ob->ob_athome && (ob->ob_state == FLYING)) {
                refuel (ob);
	}

        if ((y < MAX_Y) && (y >= 0)) {
                if ((ob->ob_state == FALLING)
		    || (ob->ob_state == WOUNDED)
		    || (ob->ob_state == WOUNDSTALL)) {
                        create_smoke (ob);
		}
                use_on_screen_buffer ();
                draw_map_object (ob);

                return plyrplane || (ob->ob_state < FINISHED);
        }

        return FALSE;
}



static void
find_SopwithObject_near_plane (SopwithObject *obp)
{
	SopwithObject *ob, *obt, *obc;
	int i, obx, obclr;

        ob = obp;
        obt = objtop + 1;

        obx = ob->ob_x;
        obclr = ob->ob_owner->ob_clr;

        for (i = 1; obt->ob_type == PLANE; ++i, ++obt) {
                if (obclr == obt->ob_owner->ob_clr) {
                        continue;
		}

                if (obt->ob_drawf == draw_enemy_plane) {
                        if ((playmode != COMPUTER)
			    || ((obx >= lcompter[i])
				&& (obx <= rcompter[i]))) {
                                if ((!(obc = compnear[i]))
				    || (abs (obx - obt->ob_x)
					< abs (obc->ob_x - obt->ob_x))) {
                                        compnear[i] = ob;
				}
			}
		}
        }
}




static gboolean
fill_counter (int *counter, 
	      int max)
{
	gboolean rc;

        rc = FALSE;
        if (*counter == max) {
                return rc;
	}
        if (max < 20) {
                if (! (countmove % 20)) {
                        ++*counter;
                        rc = plyrplane;
                }
        } else {
                *counter += max / 100;
                rc = plyrplane;
        }
        if (*counter > max) {
                *counter = max;
	}

        return rc;
}





static void
refuel (SopwithObject *obp)
{
	SopwithObject *ob;

        ob = obp;
        use_on_screen_buffer ();

        if (fill_counter (&ob->ob_life, MAXFUEL)) {
                draw_fuel_gauge (ob);
	}

        if (fill_counter (&ob->ob_rounds, MAXROUNDS)) {
                draw_shot_gauge (ob);
	}

        if (fill_counter (&ob->ob_bombs, MAXBOMBS)) {
                draw_bomb_gauge (ob);
	}

        if (fill_counter (&ob->ob_missiles, MAXMISSILES)) {
                draw_missile_gauge (ob);
	}
        if (fill_counter (&ob->ob_bursts, MAXBURSTS)) {
                draw_starburst_gauge (ob);
	}
}





int
move_shot (SopwithObject *obp)
{
	SopwithObject *ob;
	int x, y;

        ob = obp;
        delete_x (ob);
        if (!--ob->ob_life) {
                free_object (ob);
                return FALSE;
        }

        move_object (ob, &x, &y);

        if ((y >= MAX_Y) || (y <= (int) ground[x])
                || (x < 0) || (x >= MAX_X)) {
                free_object (ob);
                return FALSE;
        }

        insert_x (ob, ob->ob_xnext);
        ob->ob_newsym = (char *) 0x83;
        return TRUE;
}


int
move_bomb (SopwithObject *obp)
{
	SopwithObject *ob;
	int x, y;

        ob = obp;

        delete_x (ob);

        if (ob->ob_life < 0) {
                free_object (ob);
                ob->ob_state = FINISHED;
                use_on_screen_buffer ();
                draw_map_object (ob);
                return FALSE;
        }

        adjust_falling_object (ob);

        if (ob->ob_dy <= 0) {
#if 0
                initsound( ob, S_BOMB );
#endif
	}

        move_object (ob, &x, &y);

        if ((y < 0) || (x < 0) || (x >= MAX_X)) {
                free_object (ob);
#if 0
                stopsound (ob);
                ob->ob_state = FINISHED;
#endif
                use_on_screen_buffer ();
                draw_map_object (ob);
                return FALSE;
        }

        ob->ob_newsym = swbmbsym[find_symbol_angle (ob)];
        insert_x (ob, ob->ob_xnext);

        if (y >= MAX_Y) {
                return FALSE;
	}

        use_on_screen_buffer ();
        draw_map_object (ob);
        return TRUE;
}



static void
adjust_falling_object (SopwithObject *obp)
{
	SopwithObject *ob;

        ob = obp;
        if (!--ob->ob_life) {
                if (ob->ob_dy < 0) {
                        if (ob->ob_dx < 0) {
                                ++ob->ob_dx;
                        } else {
                                if (ob->ob_dx > 0) {
                                        --ob->ob_dx;
				}
			}
		}
		if (ob->ob_dy > -10) {
                        --ob->ob_dy;
		}
                ob->ob_life = BOMBLIFE;
        }
}



static int
find_symbol_angle (SopwithObject *ob)
{
	int dx, dy;
	
        dx = ob->ob_dx;
        dy = ob->ob_dy;
        
	if (dx == 0) {
                if (dy < 0) {
                        return 6;
                } else {
                        if (dy > 0) {
                                return 2;
                        } else {
                                return 6;
			}
		}
	} else {
                if (dx > 0) {
                        if (dy < 0) {
                                return 7;
                        } else {
                                if (dy > 0) {
                                        return 1;
                                } else {
                                        return 0;
				}
			}
                } else {
                        if (dy < 0) {
                                return 5;
                        } else {
                                if (dy > 0) {
                                        return 3;
                                } else {
                                        return 4;
				}
			}
		}
	}
}



int
move_missile (SopwithObject *obp)
{
	SopwithObject *ob;
	int x, y, angle;
	SopwithObject *obt;

        ob = obp;

        delete_x (ob);

        if (ob->ob_life < 0) {
                free_object (ob);
                ob->ob_state = FINISHED;
                use_off_screen_buffer ();
                draw_map_object (ob);
                return FALSE;
        }
	
        if (ob->ob_state == FLYING) {
                if (((obt = ob->ob_target) != ob->ob_owner)
		    && (ob->ob_life & 1)) {
                        if (obt->ob_target) {
                                obt = obt->ob_target;
			}
			
                        aim (ob, NULL, obt->ob_x, obt->ob_y, FALSE);

                        angle = ob->ob_angle
				= (ob->ob_angle + ob->ob_flaps + ANGLES) % ANGLES;
			
                        set_velocity (ob, ob->ob_speed * COS (angle),
				      ob->ob_speed * SIN (angle));
                }
		
                move_object (ob, &x, &y);
                if ((!--ob->ob_life) || (y >= ((MAX_Y * 3) / 2))) {
                        ob->ob_state = FALLING;
                        ++ob->ob_life;
                }
        } else  {
                adjust_falling_object (ob);
                ob->ob_angle = (ob->ob_angle + 1) % ANGLES;
                move_object (ob, &x, &y);
        }
	
        if ((y < 0) || (x < 0) || (x >= MAX_X)) {
                free_object (ob);
                ob->ob_state = FINISHED;
                use_on_screen_buffer ();
                draw_map_object (ob);
                return FALSE;
        }

        ob->ob_newsym = swmscsym[ob->ob_angle];
        insert_x (ob, ob->ob_xnext);

        if (y >= MAX_Y) {
                return FALSE;
	}
		
        use_on_screen_buffer ();
	draw_map_object (ob);
        return TRUE;
}


int
move_starburst (SopwithObject *obp)
{
	SopwithObject *ob;
	int x, y;

        ob = obp;
        delete_x (ob);
        if (ob->ob_life < 0) {
                ob->ob_owner->ob_target = NULL;
                free_object (ob);
                return FALSE;
        }

        adjust_falling_object (ob);
        move_object (ob, &x, &y);

        if ((y <= (int) ground[x]) || (x < 0 ) || (x >= MAX_X)) {
                ob->ob_owner->ob_target = NULL;
                free_object (ob);
                return FALSE;
        }

        ob->ob_owner->ob_target = ob;
        ob->ob_newsym = swbstsym[ob->ob_life & 1];
        insert_x (ob, ob->ob_xnext);
        return y < MAX_Y;
}



int
move_target (SopwithObject *obt)
{
	int r;
	SopwithObject *obp, *ob;

        ob = obt;
        obp = objtop;
        ob->ob_firing = NULL;
        if (gamenum && (ob->ob_state == STANDING)
	    && ((obp->ob_state == FLYING)
		|| (obp->ob_state == STALLED)
		|| (obp->ob_state == WOUNDED)
		|| (obp->ob_state == WOUNDSTALL))
	    && (ob->ob_clr != obp->ob_clr)
	    && ((gamenum > 1) || (countmove & 0x0001))
	    && ((r = range (ob->ob_x, ob->ob_y,
			    obp->ob_x, obp->ob_y)) > 0)
	    && (r < targrnge))
                fire_shot (ob, ob->ob_firing = obp);

        if (--ob->ob_hitcount < 0)
                ob->ob_hitcount = 0;

        ob->ob_newsym = (ob->ob_state == STANDING)
		? swtrgsym[ob->ob_orient]
		: swhtrsym;
        return TRUE;
}


int
move_explosion (SopwithObject *obp)
{
	SopwithObject *ob;
	int x, y;
	int orient;

        ob = obp;
        orient = ob->ob_orient;
        delete_x (ob);

        if (ob->ob_life < 0) {
#if 0
                if (orient) {
                        stopsound( ob );
		}
#endif
                free_object (ob);
                return FALSE;
        }

        if (!--ob->ob_life) {
                if (ob->ob_dy < 0) {
                        if (ob->ob_dx < 0) {
                                ++ob->ob_dx;
                        } else if (ob->ob_dx > 0) {
				--ob->ob_dx;
			}
		}

                if ((ob->ob_orient && (ob->ob_dy > -10))
		    || ((!ob->ob_orient) && (ob->ob_dy > -gminspeed))) {
                        --ob->ob_dy;
		}
		ob->ob_life = EXPLLIFE;
        }

        move_object (ob, &x, &y);

        if ((y <= (int) ground[x])
	    || (x < 0) || (x >= MAX_X)) {
#if 0
                if (orient) {
                        stopsound( ob );
		}
#endif
                free_object (ob);

                return FALSE;
        }
        ++ob->ob_hitcount;

        insert_x (ob, ob->ob_xnext);
        ob->ob_newsym = swexpsym[ob->ob_orient];
        return y < MAX_Y;
}


int
move_smoke (SopwithObject *obp)
{
	SopwithObject *ob;
	int state;

        ob = obp;
        if ((!--ob->ob_life)
	    || (((state = ob->ob_owner->ob_state) != FALLING)
		&& (state != WOUNDED)
		&& (state != WOUNDSTALL)
		&& (state != CRASHED))) {
                free_object (ob);
                return FALSE;
        }
        ob->ob_newsym = (char *) (long)(0x80 + ob->ob_clr);

        return TRUE;
}


int
move_flock (SopwithObject *obp)
{
	SopwithObject *ob;
	int x, y;

        ob = obp;
        delete_x (ob);

        if (ob->ob_life == -1) {
                use_on_screen_buffer ();
                draw_map_object (ob);
                free_object (ob);
                return FALSE;
        }

        if (!--ob->ob_life) {
                ob->ob_orient = !ob->ob_orient;
                ob->ob_life = FLOCKLIFE;
        }

        if ((ob->ob_x < MINFLCKX) || (ob->ob_x > MAXFLCKX)) {
                ob->ob_dx = -ob->ob_dx;
	}

        move_object (ob, &x, &y);
        insert_x (ob, ob->ob_xnext);
        ob->ob_newsym = swflksym[ob->ob_orient];
        use_on_screen_buffer ();
        draw_map_object (ob);
        return TRUE;
}


int
move_bird (SopwithObject *obp)
{
	SopwithObject *ob;
	int x, y;

        ob = obp;

        delete_x (ob);

        if (ob->ob_life == -1) {
                free_object (ob);
                return FALSE;
        } else if (ob->ob_life == -2) {
                ob->ob_dy = -ob->ob_dy;
                ob->ob_dx = (countmove & 7) - 4;
                ob->ob_life = BIRDLIFE;
        } else if (!--ob->ob_life) {
                ob->ob_orient = !ob->ob_orient;
                ob->ob_life = BIRDLIFE;
        }

        move_object (ob, &x, &y);

        insert_x (ob, ob->ob_xnext);

        ob->ob_newsym = swbrdsym[ob->ob_orient];

        if ((y >= MAX_Y ) || (y <= (int) ground[x])
                || (x < 0) || (x >= MAX_X)) {
                ob->ob_y -= ob->ob_dy;
                ob->ob_life = -2;
                return FALSE;
        }
        return TRUE;
}



int
move_ox (SopwithObject *ob)
{
        ob->ob_newsym = swoxsym[ob->ob_state != STANDING];
        return TRUE;
}


void
crash_plane (SopwithObject *obp)
{
	SopwithObject *ob, *obo;

        ob = obp;

        if (ob->ob_dx < 0) {
                ob->ob_angle = (ob->ob_angle + 2) % ANGLES;
	} else {
                ob->ob_angle = (ob->ob_angle + ANGLES - 2) % ANGLES;
	}

        ob->ob_state = (ob->ob_state >= GHOST) ? GHOSTCRASHED : CRASHED;
        ob->ob_athome = FALSE;
        ob->ob_dx = ob->ob_dy = ob->ob_ldx = ob->ob_ldy = ob->ob_speed = 0;

        obo = &oobjects[ob->ob_index];

        ob->ob_hitcount = ((abs( obo->ob_x - ob->ob_x) < SAFERESET)
                         && (abs( obo->ob_y - ob->ob_y) < SAFERESET))
                         ? (MAXCRCOUNT << 1) : MAXCRCOUNT;
}



void
hit_plane (SopwithObject *obp)
{
	SopwithObject *ob;

        ob = obp;
        ob->ob_ldx = ob->ob_ldy = 0;
        ob->ob_hitcount = FALLCOUNT;
        ob->ob_state = FALLING;
        ob->ob_athome = FALSE;
}



static void 
stall_plane (SopwithObject *obp)
{
	SopwithObject *ob;

        ob = obp;
        ob->ob_ldx = ob->ob_ldy = ob->ob_orient = ob->ob_dx = 0;
        ob->ob_angle = 7 * ANGLES / 8;
        ob->ob_speed = 0;
        ob->ob_dy = 0;
        ob->ob_hitcount = STALLCOUNT;
        ob->ob_state = (ob->ob_state >= GHOST) ? GHOSTSTALLED :
                       ((ob->ob_state == WOUNDED) ? WOUNDSTALL : STALLED);
        ob->ob_athome = FALSE;
}



void
insert_x (SopwithObject *ob, 
	  SopwithObject *obp)
{
        SopwithObject *obs;
        int      obx;

        obs = obp;
        obx = ob->ob_x;
        if (obx < obs->ob_x) {
                do {
		        obs = obs->ob_xprev;
                } while (obx < obs->ob_x);
        } else {
                while (obx >= obs->ob_x)
                        obs = obs->ob_xnext;
                obs = obs->ob_xprev;
        }

        ob->ob_xnext = obs->ob_xnext;
        ob->ob_xprev = obs;
        obs->ob_xnext->ob_xprev = ob;
        obs->ob_xnext = ob;
}




void
delete_x (SopwithObject *obp)
{
        SopwithObject *ob;

        ob = obp;
        ob->ob_xnext->ob_xprev = ob->ob_xprev;
        ob->ob_xprev->ob_xnext = ob->ob_xnext;
}



void
move_object (SopwithObject *ob,
	     int     *x,
	     int     *y)
{
	ob->ob_x = ob->ob_x + ob->ob_dx;
	ob->ob_lx = ob->ob_lx + ob->ob_ldx;
	*x = ob->ob_x;

	ob->ob_y = ob->ob_y + ob->ob_dy;
	ob->ob_ly = ob->ob_ly + ob->ob_ldy;
	*y = ob->ob_y;
}
