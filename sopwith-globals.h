/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */


#ifndef SOPWITH_GLOBALS_H
#define SOPWITH_GLOBALS_H

#include "sw.h"

#include "sopwith-object.h"

extern  SopwithPlayMode         playmode;          /* Play mode */

extern  SopwithGameParameters   swgames[];         /* Game parameters */
extern  SopwithGameParameters  *currgame;          /* Current game */
extern  GRNDTYPE                ground[];          /* Ground height by pixel           */
extern  GRNDTYPE                orground[];        /* Original ground height by pixel  */
extern  MULTIO                 *multbuff;          /* Communications buffer            */
extern  gboolean                hires;             /* High res flag                    */
extern  SopwithObject           topobj;            /* Top and Bottom of object by x lst*/
extern  SopwithObject           botobj;

#endif /* SOPWITH_GLOBALS_H */
