/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
        swinit   -      SW initialization

                        Copyright (C) 1984-2000 David L. Clark.

                        All rights reserved except as specified in the
                        file license.txt.  Distribution of this file
                        without the license.txt file accompanying is
                        prohibited.

                        Author: Dave Clark

        Modification History:
                        84-02-02        Development
                        85-10-31        Atari
                        87-01-09        BMB standard help text.
                                        Multiple serial ports.
                        87-03-09        Microsoft compiler.
                        87-03-11        Smaller fuel tank explosions.
                        87-03-12        Wounded airplanes.
                        87-03-30        Novice player.
                        87-03-31        Missiles
                        87-03-31        Less x-movement in explosions
                        87-04-04        Missile and starburst support
                        87-04-05        Less x-movement in explosions
                        87-04-06        Computer plane avoiding oxen.
                        87-04-09        Fix to initial missile path.
                                        Delay between starbursts
                        96-12-26        New network version.
                                        Remove keyboard prompts.
                                        Speed up game a bit.
                        99-01-24        1999 copyright.
                                        Disable network support.
                        2000-10-29      Copyright update.
                                        Comment out multiplayer selection
                                          on startup.
			2000-12-14      Started GNOME porting. See
                                          ChangeLog for further changes.  
*/

#include        <config.h>
#include        "sopwith-init.h"


#include "sw.h"
#include "sopwith-collision.h"
#include "sopwith-draw.h"
#include "sopwith-object.h"
#include "sopwith-globals.h"
#include "sopwith-graphics.h"
#include "sopwith-title.h"
#include "sopwith-move.h"

#include        <gtk/gtkmain.h>
#include        <ctype.h>
#include        <setjmp.h>
#include        <stdio.h>
#include        <stdlib.h>
#include        <string.h>
#include        <time.h>
#include        <unistd.h>


#define LIVES_GAUGE_X       (SCR_CENTR - 25) /*  Crash, fuel, bomb, shot, missile*/
#define FUEL_GAUGE_X        (SCR_CENTR - 22) /*     and starburst guage          */
#define BOMB_GAUGE_X        (SCR_CENTR - 19) /*     X-coordinates                */
#define SHOT_GAUGE_X        (SCR_CENTR - 16)
#define MISSILE_GAUGE_X     (SCR_CENTR - 13)
#define STARBURST_GAUGE_X   (SCR_CENTR - 10)

#define GHOST_X             (SCR_CENTR - 21)/*  Ghost face display x-coodinate  */


extern  gboolean  titleflg;               /* Title flag                       */
extern  gboolean  disppos;                /* Display position flag            */
extern  int       keydelay;               /*  Number of displays per keystroke*/
extern  int       soundflg;               /*  Sound flag                      */
extern  gboolean  repflag;                /*  Report statistics flag          */
extern  gboolean  joystick;               /*  Joy stick being used            */
extern  int       multtick;               /*  Multiple user tick delay        */
extern  gboolean  inplay;                 /*  Currently playing flag          */
extern  int       koveride;               /* Keyboard override index number   */
extern  int       missok;                 /* Missiles supported               */

extern  int       gamenum;                /* Current game number              */
extern  int       gmaxspeed, gminspeed;   /* Speed range based on game number */
extern  int       targrnge;               /* Target range based on game number*/

extern  int       displx, disprx;         /* Display left and right           */
extern  char      auxdisp[];              /* Auxiliary display area           */
extern  gboolean  dispinit;               /* Initalized display flag.         */
extern  char     *histin, *histout;       /* History input and output files   */

extern  SopwithObject  *nobjects;               /* SopwithObject list.                    */
extern  SopwithObject   oobjects[];             /* Original plane object description*/
extern  SopwithObject  *objbot, *objtop,        /* Top and bottom of object list    */
                 *objfree,                /* Free list                        */
                 *deltop, *delbot;        /* Newly deallocated SopwithObject        */
extern  SopwithObject  *targets[];              /* Target status array              */
extern  int       numtarg[];              /* number of active targets         */
extern  SopwithObject   topobj, botobj;         /* Top and Bottom of object by x lst*/
extern  int       player;                 /* Pointer to player's object       */
extern  gboolean  plyrplane;              /* Current object is the player     */
extern  gboolean  compplane;              /* Current object is a comp plane   */
extern  int       currobx;                /* Current object index             */
extern  OLDWDISP  wdisp[];                /* World display status             */
extern  int       splatox;                /* Display splatted ox              */
extern  int       oxsplatted;             /* An ox has been splotted          */
extern  int       movetick, movemax;      /* Move timing                      */

extern  int       counttick, countmove;   /* Performance counters   */


extern  int       sintab[];               /*  sine table based on angles    */

extern  jmp_buf   envrestart;             /* Restart environment for restart  */
                                          /*  long jump.                      */

extern  int       endsts[];               /* End of game status and move count*/
extern  int       endcount;
extern  gboolean  goingsun;               /* Heading for the sun flag         */
extern  SopwithObject  *compnear[];             /*  Array of planes near computers*/
extern  unsigned   explseed;              /* explosion seed                 */

extern  char     *multfile;               /* Multi user files               */
extern  char     *cmndfile;
extern  unsigned  multaddr;               /* Multiple user diskette adapter */
                                          /*   address                      */

extern  int       maxcrash;               /* Maximum number of crashes      */

static  int      savescore;               /* save players score on restart  */
static  gboolean     ghost;               /* ghost display flag             */

extern  gboolean  paused;

#if 0
static  char     *helptxt[] = {         /* Help text                      */
"",
"SOPWITH, Distribution Version",
"(c) Copyright 1984-2000 David L. Clark",
"Modification Date:  October 29, 2000",
"",
"Usage:  sopwith [options]",
"The options are:",
"        -n :  novice single player",
"        -s :  single player",
"        -c :  single player against computer",
/*
"        -m :  multiple players on a network",
*/
"        -a :  2 players over asynchrounous communications line",
"              (Only one of -n, -s, -c, -a may be specified)",
"        -k :  keyboard only",
"        -j :  joystick and keyboard",
"              (Only one of -k and -j  may be specified)",
/*
"        -i :  IBM PC keyboard",
*/
"        -q :  begin game with sound off",
/*
"        -r :  resets the multiuser communications file after an abnormal",
"                  end of a game",
"        -d*:  overrides the default drive C as the device containing the",
"                  communications file",
*/
"        -p#:  overrides asynchronous port 1 as the asynchrounous port",
"                  to use",
0
};
#endif


static gboolean clock_tick_handler (void *data);
static void     initialize_seed    (void);
#if 0
static void     get_play_mode      (void);
#endif
static void     create_ground      (void);
static void     create_targets     (void);
static void     show_scores        (void);
static int      distance           (int   x,
				    int   y, 
				    int   ax, 
				    int   ay);
static void     create_oxen        (void);
static void     create_flocks      (void);

static void     draw_gauge         (int   x,
				    int   cury, 
				    int   maxy, 
				    int   clr);
static void     draw_world         (void);

static void     copy_off_screen_buffer_to_screen (void);
static void     clear_off_screen_buffer          (void);
#if 0
static void     clear_text                       (void);
#endif
static void     set_difficulty                   (void);
static void     draw_map_targets                 (void);


void
sopwith_init (void)
{
	int     modeset = 0;
	int     keyset = 0;

        nobjects = g_new0 (SopwithObject, 100);

	
        soundflg = !soundflg;

        if (modeset && keyset) {
                titleflg = TRUE;
	}

        movemax = 15;
        initialize_seed ();

#if 0 /* FIXME: figure out history crap */
        explseed = histinit (explseed);
#endif
 
#if 0 /* FIXME: sound */
	initsndt ();
#endif
        create_ground ();
	
#if 0
        if (modeset) {
                playmode = n ? NOVICE
			: ( s ? SINGLE
			    : ( c ? COMPUTER
				: ( m ? MULTIPLE : ASYNCH )));
        } else {
                get_play_mode ();
	}

#if 0 /* FIXME: no joystick support right now */
        if (!keyset) {
                get_control_type ();
	}
#endif
	
#if 0 /* FIXME: add multiplayer support (over network) */
        if ((playmode == MULTIPLE) || (playmode == ASYNCH)) {
                maxcrash = MAXCRASH * 2;
                if (playmode == MULTIPLE) {
                        init1mul (reset, device);
                } else {
                        init1asy ();
		}
                create_ground ();
                initialize_SopwithObject ();
                if (playmode == MULTIPLE)
                        init2mul ();
                else
                        init2asy ();
                create_targets ();

                if (currgame->gm_specf) {
                        (*currgame->gm_specf) ();
		}
                initialize_display (FALSE);
                if (keydelay == -1) {
                        keydelay = 1;
		}
        } else {

        }
#endif


	keydelay = 1;
	maxcrash = MAXCRASH;
	currgame = &swgames[0];
	
	clear_text ();
	
	initialize_SopwithObject ();
	create_player_plane (NULL);
	create_enemy_plane (NULL);
	create_enemy_plane (NULL);
	create_enemy_plane (NULL);
	create_targets ();
	
	initialize_display (FALSE);


        create_flocks ();
        create_oxen ();

        set_difficulty ();

	/* FIXME: set up keyboard */

        inplay = TRUE;

	gtk_timeout_add (55, clock_tick_handler, NULL);
#endif
}


#if 0
static void
show_help (char **hp)
{
	char **h;
 
	h = hp;
        puts( "\r\n" );
        while ( *h ) {
                puts( *h++ );
                puts( "\r\n" );
        }
}
#endif



static void
initialize_seed (void)
{
	srand (getpid () * time (NULL));
	explseed = rand ();
}




void
sopwith_restart (void)
{
	SopwithObject  *ob;
	int      inc;
#if 0
	int      tickwait;
#endif

        if (endsts[player] == WINNER) {
                ob = &nobjects[player];
                inc = 0;
                while (ob->ob_crashcnt++ < maxcrash) {
                        ob->ob_score += (inc += 25);
                        use_on_screen_buffer ();
                        draw_lives_gauge (ob);
			draw_score (ob);
#if 0 /* wait 5 ticks */
                        intsoff ();
                        tickwait = 5;
                        counttick = 0;
                        intson();
                        while ( counttick < tickwait );
#endif
                }
                ++gamenum;
                savescore = ob->ob_score;
        } else {
                gamenum = 0;
                savescore = 0;
        }

#if 0
        initsndt();
#endif

        create_ground ();
        initialize_objects ();

        create_player_plane (NULL);

        create_enemy_plane (NULL);
        create_enemy_plane (NULL);
        create_enemy_plane (NULL);
	create_targets ();

        initialize_display (FALSE);

        create_flocks ();
        create_oxen ();

        set_difficulty ();

        longjmp (envrestart, 0);
}


static void
set_difficulty (void)
{

        gmaxspeed = MAX_SPEED + gamenum;
        gminspeed = MIN_SPEED + gamenum;

        targrnge = 150;
        if (gamenum < 6)
                targrnge -= 15 * (6 - gamenum);
        targrnge *= targrnge;
}




#if 0
static void
clear_text (void)
{
#if 0 /* FIXME: Need code to clear text area */
	register int    i;
	struct  regval  reg;

        for (i = 0; i < 4; ++i) {
                swposcur (0, 20 + i);
                reg.axr = 0x0A20;
                reg.bxr = 0;
                reg.cxr = hires ? 80 : 40;
                sysint (0x10, &reg, &reg);
        }
        swposcur (0, 20);
#endif

}
#endif


#if 0
static void
get_control_type (void)
{
	char    key;
	
        /*----------------97/12/27--------------
        clear_text ();
        puts ("Key: 1 - Joystick with IBM Keyboard\r\n");
        puts ("     2 - Joystick with non-IBM Keyboard\r\n");
        puts ("     3 - IBM Keyboard only\r\n" );
        puts ("     4 - Non-IBM keyboard only\r\n");

        while (1) {
		if (((key = swgetc () & 0x00FF) < '1') {
		    || (key > '4')) {
                        continue;
		}
                joystick = (key <= '2');
                return;
        }
        ------------------97/12/27--------------*/

        clear_text ();

        puts ("Key: K - Keyboard Only\r\n");
        puts ("     J - Joystick and Keyboard\r\n");

        while (1) {
		key = getc (stdin);
		
		key = toupper (key);

		if (key != 'K' && key != 'J') {
                        continue;
		}

                joystick = (key == 'J');

                return;
        }
}
#endif



#if 0
static void
get_play_mode (void)
{
	int key;

	clear_text ();
        puts ("Key: S - single player\r\n");
        puts ("     C - single player against computer\r\n");

#if 0
	/*--------------- 2000/10/29 -----------------------
	  puts( "     M - multiple players on network\r\n" );
	  ----------------- 2000/10/29 ---------------------*/
	puts ("     A - 2 players on asynchronous line");
#endif

        while (1) {
		key = getc (stdin);
		key = toupper (key);

                switch (key) {
		case 'S':
			clear_text ();
			puts( "Key: N - novice player\r\n" );
			puts( "     E - expert player\r\n" );
			key = getc (stdin);
			key = toupper (key);
			
			while (1) {
				switch (key) {
				case 'N':
					playmode = NOVICE;
					return;
				case 'E':
						playmode = SINGLE;
						return;
				}
			}
		case 'M':
                        /*----- 2000/10/29 ---------- 
			  playmode
			  = MULTIPLE; return; -------
			  2000/10/29 --------*/
			break;
		case 'C':
			playmode = COMPUTER;
			return;
		case 'A':
			/*----- 2000/10/29 ---------- 
			  playmode = ASYNCH;
			  return;
			  -------------------*/
			break;
		default:
			break;
                }
        }
}
#endif



static void
create_ground (void)
{
        memmove (ground, orground, sizeof (GRNDTYPE) * MAX_X);
}



extern   char    swghtsym[];

void
initialize_display (gboolean reset)
{
	SopwithObject *ob;
	SopwithObject          ghostob;

        splatox = oxsplatted = 0;
        if (!reset) {
                clear_off_screen_buffer ();
                use_off_screen_buffer ();
                draw_world ();
                stop_playing_title_song ();
                ghost = FALSE;
        }
	
        copy_off_screen_buffer_to_screen ();
        use_on_screen_buffer ();
        draw_map_targets ();
        show_scores ();

        ob = &nobjects[player];
        if (ghost) {
                ghostob.ob_type = DUMMYTYPE;
                ghostob.ob_symhgt = ghostob.ob_symwdt = 8;
                ghostob.ob_clr = ob->ob_clr;
                ghostob.ob_newsym = swghtsym;
                draw_sprite (GHOST_X, 12, &ghostob);
        } else {
                draw_fuel_gauge (ob);
                draw_bomb_gauge (ob);
                draw_missile_gauge (ob);
                draw_starburst_gauge (ob);
                draw_shot_gauge (ob);
                draw_lives_gauge (ob);
        }
        dispinit = TRUE;
}



static void
show_scores (void)
{
        if (savescore) {
		nobjects[0].ob_score = savescore;
		savescore = 0;
        }

        draw_score (&nobjects[0]);
        if (((playmode == MULTIPLE) || (playmode == ASYNCH))
	    && (multbuff->mu_maxplyr > 1))
                draw_score (&nobjects[1]);
}




void
draw_lives_gauge (SopwithObject *ob)
{
        draw_gauge (LIVES_GAUGE_X, maxcrash - ob->ob_crashcnt, maxcrash, ob->ob_clr);
}



void
draw_fuel_gauge (SopwithObject *ob)

{
        draw_gauge (FUEL_GAUGE_X, ob->ob_life >> 4, MAXFUEL >> 4, ob->ob_clr);
}


void
draw_bomb_gauge (SopwithObject *ob)
{
        draw_gauge (BOMB_GAUGE_X, ob->ob_bombs, MAXBOMBS, 3 - ob->ob_clr);
}



void
draw_shot_gauge (SopwithObject *ob)
{
        draw_gauge (SHOT_GAUGE_X, ob->ob_rounds, MAXROUNDS, 3);
}





void
draw_missile_gauge (SopwithObject *ob)
{
        draw_gauge (MISSILE_GAUGE_X, ob->ob_missiles, MAXMISSILES, ob->ob_clr);
}



void
draw_starburst_gauge (SopwithObject *ob)
{
        draw_gauge (STARBURST_GAUGE_X, ob->ob_bursts, MAXBURSTS, 3 - ob->ob_clr);
}




static void
draw_gauge (int x,
	    int cury, 
	    int maxy, 
	    int clr)
{
	int y;

        if (ghost)
                return;

        cury = cury * 10 / maxy - 1;

        if (cury > 9)
                cury = 9;

        for (y = 0; y <= cury; y++)
                draw_point (x, y, clr);

        while (y <= 9) {
		draw_point (x, y, 0);
		y++;
	}
}



static void
draw_world (void)
{
	int     x, y, dx, maxh, sx;

        dx = 0;
        sx = SCR_CENTR;

        maxh = 0;
        y = 0;
        for (x = 0; x < MAX_X; ++x) {

                if (ground[x] > maxh) {
                        maxh = ground[x];
		}

                if ((++dx) == WRLD_RSX) {
                        maxh /= WRLD_RSY;
                        if (maxh == y) {
                                draw_point (sx, maxh, 7);
                        } else {
                                if (maxh > y) {
                                        for (y++; y <= maxh; y++) {
                                                draw_point (sx, y, 7);
					}
                                } else {
                                        for (y--; y >= maxh; y--) {
                                                draw_point (sx, y, 7);
					}
				}
			}
                        y = maxh;
                        draw_point (sx, 0, 11);
                        sx++;
                        dx = maxh = 0;
                }
        }
        maxh = MAX_Y / WRLD_RSY;
        for (y = 0; y <= maxh; y++) {
                draw_point (SCR_CENTR, y, 11);
                draw_point (sx, y, 11);
        }

        for (x = 0; x < SCR_WDTH; ++x) {
                draw_point (x, (SCR_MNSH + 2), 7);
	}
}





static void
draw_map_targets (void)
{
	int     x;
	SopwithObject *ob;
	OLDWDISP *ow;

        ow = wdisp;
        ob = nobjects;
        for (x = 0; x < MAX_OBJS; x++, ow++, ob++) {
                ow->ow_xorplot = ob->ob_drwflg = ob->ob_delflg = 0;
	}

        for (x = 0; x < MAX_TARG; x++) {
                if ((ob = targets[x]) && (ob->ob_state != FINISHED)) {
                        draw_map_object (ob);
		}
	}

}



static void
copy_off_screen_buffer_to_screen (void)
{
	/* Non-portable drawing code */
#if 0
#ifdef IBMPC
        swsetblk( 0,        SCR_SEGM, 0x1000, 0 );
        swsetblk( SCR_ROFF, SCR_SEGM, 0x1000, 0 );
        movblock( auxdisp,          dsseg(), 0x1000, SCR_SEGM, 0x1000 );
        movblock( auxdisp + 0x1000, dsseg(), 0x3000, SCR_SEGM, 0x1000 );
#endif

#ifdef ATARI
long    trap14(), vidram;

        vidram = trap14( 3 );
        setmem( vidram, 0x4000, 0 );
        movmem( auxdisp, vidram + 0x4000, 0x4000 );
#endif
#endif
}




static void
clear_off_screen_buffer (void)
{
#if 0
#ifdef  IBMPC
        setmem( auxdisp, 0x2000, 0 );
#endif

#ifdef  ATARI
        setmem( auxdisp, 0x4000, 0 );
#endif
#endif
}




static  int     inits[2] = { 0, 7 };
static  int     initc[4] = { 0, 7, 1, 6 };
static  int     initm[8] = { 0, 7, 3, 4, 2, 5, 1, 6 };



SopwithObject *
create_plane (SopwithObject *obp)
{
	SopwithObject *ob;
	int x, height, minx, maxx;
	int n = 0;

        if (!obp) {
                ob = allocate_object ();
        } else {
                ob = obp;
	}

        switch (playmode) {
                case SINGLE:
                case NOVICE:
                        n = inits[ob->ob_index];
                        break;
                case MULTIPLE:
                case ASYNCH:
                        n = initm[ob->ob_index];
                        break;
                case COMPUTER:
                        n = initc[ob->ob_index];
                        break;
        }

        ob->ob_type = PLANE;

        ob->ob_x = currgame->gm_x[n];
        minx = ob->ob_x;
        maxx = ob->ob_x + 20;
        height = 0;
        for (x = minx; x <= maxx; ++x) {
                if (ground[x] > height) {
                        height = ground[x];
		}
	}

        ob->ob_y = height + 13;
        ob->ob_lx = ob->ob_ly = ob->ob_speed = ob->ob_flaps = ob->ob_accel
                  = ob->ob_hitcount = ob->ob_bdelay = ob->ob_mdelay
                  = ob->ob_bsdelay = 0;
        set_velocity (ob, 0, 0);
        ob->ob_orient = currgame->gm_orient[n];
        ob->ob_angle = (ob->ob_orient) ? (ANGLES / 2) : 0;
        ob->ob_target = ob->ob_firing = ob->ob_mfiring = NULL;
        ob->ob_bombing = ob->ob_bfiring = ob->ob_home = FALSE;
        ob->ob_symhgt = SYM_HGHT;
        ob->ob_symwdt = SYM_WDTH;
        ob->ob_athome = TRUE;

        if ((!obp) || (ob->ob_state == CRASHED)
	    || (ob->ob_state == GHOSTCRASHED)) {
                ob->ob_rounds = MAXROUNDS;
                ob->ob_bombs = MAXBOMBS;
                ob->ob_missiles = MAXMISSILES;
                ob->ob_bursts = MAXBURSTS;
                ob->ob_life = MAXFUEL;
        }

        if (!obp) {
                ob->ob_score = ob->ob_updcount = ob->ob_crashcnt
			= endsts[ob->ob_index] = 0;
                compnear[ob->ob_index] = NULL;
                insert_x (ob, &topobj);
        } else {
                delete_x (ob);
                insert_x (ob, ob->ob_xnext);
        }

        if (((playmode == MULTIPLE) || (playmode == ASYNCH))
	    && (ob->ob_crashcnt >= maxcrash)) {
                ob->ob_state = GHOST;
                if (ob->ob_index == player)
                        ghost = TRUE;
        } else {
                ob->ob_state = FLYING;
	}

        return ob;
}




void
create_enemy_plane (SopwithObject *obp)
{
	SopwithObject *ob;

        ob = create_plane (obp);
        if (!obp) {
                ob->ob_drawf = draw_enemy_plane;
                ob->ob_movef = move_enemy_plane;
                ob->ob_clr = 2;
                if ((playmode != MULTIPLE) && (playmode != ASYNCH)) {
                        ob->ob_owner = &nobjects[1];
                } else {
                        if (ob->ob_index == 1) {
                                ob->ob_owner = ob;
                        } else {
                                ob->ob_owner = ob - 2;
			}
		}

		memmove (&oobjects[ob->ob_index], ob, sizeof (SopwithObject));
        }

        if ((playmode == SINGLE) || (playmode == NOVICE)) {
		ob->ob_state = FINISHED;
		delete_x (ob);
        }
}



void
create_player_plane (SopwithObject *obp)
{
	SopwithObject *ob;

        ob = create_plane (obp);
        if (!obp) {
                ob->ob_drawf = draw_player_plane;
                ob->ob_movef = move_player_plane;
                ob->ob_clr = ob->ob_index % 2 + 1;
                ob->ob_owner = ob;

                memmove (&oobjects[ob->ob_index], ob, sizeof (SopwithObject));

                goingsun = FALSE;
                endcount = 0;
        }

        displx = ob->ob_x - SCR_CENTR;
        disprx = displx + SCR_WDTH - 1;

#if 0 /* Flush keyboard input queue - is this really the right place to do so? */
        swflush ();
#endif
}





void
fire_shot (SopwithObject *obo, SopwithObject *target)
{
	SopwithObject *ob;
	int      nangle, nspeed, dx, dy, r, bspeed, x, y;

        if ((!target) && (!compplane) && (!obo->ob_rounds)) {
                return;
	}

	ob = allocate_object ();

        if (!ob) {
                return;
	}

        if (playmode != NOVICE) {
                obo->ob_rounds--;
	}

        bspeed = BULSPEED + gamenum;

        if (target) {
                x = target->ob_x + (target->ob_dx << 2);
                y = target->ob_y + (target->ob_dy << 2);
                dx = x - obo->ob_x;
                dy = y - obo->ob_y;
                if ((r = distance (x, y, obo->ob_x, obo->ob_y)) < 1) {
                        free_object (ob);
                        return;
                }
                ob->ob_dx = (dx * bspeed) / r;
                ob->ob_dy = (dy * bspeed) / r;
                ob->ob_ldx = ob->ob_ldy = 0;
        } else {
                nspeed = obo->ob_speed + bspeed;
                nangle = obo->ob_angle;
                set_velocity (ob, 
			      nspeed * COS (nangle),
			      nspeed * SIN (nangle));
        }

        ob->ob_type = SHOT;
        ob->ob_x = obo->ob_x + SYM_WDTH / 2;
        ob->ob_y = obo->ob_y - SYM_HGHT / 2;
        ob->ob_lx = obo->ob_lx;
        ob->ob_ly = obo->ob_ly;

        ob->ob_life = BULLIFE;
        ob->ob_owner = obo;
        ob->ob_clr = obo->ob_clr;
        ob->ob_symhgt = ob->ob_symwdt = 1;
        ob->ob_drawf = NULL;
        ob->ob_movef = move_shot;
        ob->ob_speed = 0;

        insert_x (ob, obo);
}


static int
distance (int x,
	  int y, 
	  int ax, 
	  int ay)
{
	int dx, dy, tmp;

        dy = abs (y - ay);
        dy += dy >> 1;
	dx = abs (x - ax);

        if (dx > 100 || dy > 100) {
                return  -1;
	}

        if (dx < dy) {
                tmp = dx;
                dx = dy;
                dy = tmp;
        }

        return (((7 * dx) + (dy << 2)) >> 3);
}


void
drop_bomb (SopwithObject *obop)
{
	SopwithObject *ob, *obo;
	int      angle;

        obo = obop;
        if (((!compplane) && (!obo->ob_bombs)) || (obo->ob_bdelay)) {
                return;
	}

	ob = allocate_object ();

	if (!ob) {
                return;
	}

        if (playmode != NOVICE) {
                --obo->ob_bombs;
	}

        obo->ob_bdelay = 10;

        ob->ob_type = BOMB;
        ob->ob_state = FALLING;
        ob->ob_dx = obo->ob_dx;
        ob->ob_dy = obo->ob_dy;

        if (obo->ob_orient) {
                angle = (obo->ob_angle + (ANGLES / 4)) % ANGLES;
	} else {
                angle = (obo->ob_angle + (3 * ANGLES / 4)) % ANGLES;
	}

	ob->ob_x = obo->ob_x + ((COS (angle) * 10) >> 8) + 4;
        ob->ob_y = obo->ob_y + ((SIN (angle) * 10) >> 8) - 4;
        ob->ob_lx = ob->ob_ly = ob->ob_ldx = ob->ob_ldy = 0;

        ob->ob_life = BOMBLIFE;
        ob->ob_owner = obo;
        ob->ob_clr = obo->ob_clr;
        ob->ob_symhgt = ob->ob_symwdt = 8;
        ob->ob_drawf = draw_bomb;
        ob->ob_movef = move_bomb;

        insert_x (ob, obo);
}


void
fire_missile (SopwithObject *obop)
{
	SopwithObject *ob, *obo;
	int      angle, nspeed;

        obo = obop;
        if (obo->ob_mdelay || (!obo->ob_missiles) || !missok)
                return;
        if (!(ob = allocate_object ())) {
                return;
	}

        if (playmode != NOVICE) {
                --obo->ob_missiles;
	}

        obo->ob_mdelay = 5;

        ob->ob_type = MISSILE;
        ob->ob_state = FLYING;

        angle = ob->ob_angle = obo->ob_angle;
        ob->ob_x = obo->ob_x + (COS(angle) >> 4) + 4;
        ob->ob_y = obo->ob_y + (SIN(angle) >> 4) - 4;
        ob->ob_lx = ob->ob_ly = 0;
        ob->ob_speed = nspeed = gmaxspeed + (gmaxspeed >> 1);
        set_velocity (ob, nspeed * COS (angle), nspeed * SIN (angle));

        ob->ob_life = MISSLIFE;
        ob->ob_owner = obo;
        ob->ob_clr = obo->ob_clr;
        ob->ob_symhgt = ob->ob_symwdt = 8;
        ob->ob_drawf = draw_missile;
        ob->ob_movef = move_missile;
        ob->ob_target = obo->ob_mfiring;
        ob->ob_orient = ob->ob_accel = ob->ob_flaps = 0;

        insert_x (ob, obo);
}



void
fire_starburst (SopwithObject *obop)
{
	SopwithObject *ob, *obo;
	int      angle;

        obo = obop;
        if (obo->ob_bsdelay || (!obo->ob_bursts) || !missok) {
                return;
	}

        if (!(ob = allocate_object ())) {
                return;
	}

        ob->ob_bsdelay = 5;

        if (playmode != NOVICE) {
                --obo->ob_bursts;
	}

        ob->ob_type = STARBURST;
        ob->ob_state = FALLING;

        if (obo->ob_orient) {
                angle = (obo->ob_angle + (3 * ANGLES / 8)) % ANGLES;
	} else {
                angle = (obo->ob_angle + (5 * ANGLES / 8)) % ANGLES;
	}

        set_velocity (ob, gminspeed * COS (angle), gminspeed * SIN (angle));
        ob->ob_dx += obo->ob_dx;
        ob->ob_dy += obo->ob_dy;

        ob->ob_x = obo->ob_x + ((COS (angle) * 10) >> 10) + 4;
        ob->ob_y = obo->ob_y + ((SIN (angle) * 10) >> 10) - 4;
        ob->ob_lx = ob->ob_ly = 0;

        ob->ob_life = BURSTLIFE;
        ob->ob_owner = obo;
        ob->ob_clr = obo->ob_clr;
        ob->ob_symhgt = ob->ob_symwdt = 8;
        ob->ob_drawf = draw_starburst;
        ob->ob_movef = move_starburst;

        insert_x (ob, obo);
}



static void
create_targets (void)
{
	SopwithObject *ob;
	int x, i;
	int *tx, *tt;
	int minh, maxh, aveh, minx, maxx;

        tx = currgame->gm_xtarg;
        tt = currgame->gm_ttarg;

        if (((playmode != MULTIPLE) && (playmode != ASYNCH))
                || (multbuff->mu_maxplyr == 1 )) {
                numtarg[0] = 0;
                numtarg[1] = MAX_TARG - 3;
        } else {
                numtarg[0] = numtarg[1] = MAX_TARG / 2;
	}

        for (i = 0; i < MAX_TARG; ++i, ++tx, ++tt) {
                targets[i] = ob = allocate_object ();
                minx = ob->ob_x = *tx;
                maxx = ob->ob_x + 15;
                minh = 999;
                maxh = 0;
                for (x = minx; x <= maxx; ++x) {
                        if (ground[x] > maxh) {
                                maxh = ground[x];
			} if ( ground[x] < minh ) {
                                minh = ground[x];
			}
                }
		
                aveh = (minh + maxh) >> 1;

                while ((ob->ob_y = aveh + 16 ) >= MAX_Y) {
                        --aveh;
		}

                for (x = minx; x <= maxx; ++x) {
                        ground[x] = aveh;
		}

                ob->ob_dx = ob->ob_dy = ob->ob_lx = ob->ob_ly = ob->ob_ldx
			= ob->ob_ldy = ob->ob_angle = ob->ob_hitcount = 0;
                ob->ob_type = TARGET;
                ob->ob_state = STANDING;
                ob->ob_orient = *tt;
                ob->ob_life = i;

                if (((playmode != MULTIPLE) && (playmode != ASYNCH))
		    || (multbuff->mu_maxplyr == 1)) {
                        ob->ob_owner = &nobjects[( ( i < MAX_TARG / 2 )
						   && ( i > MAX_TARG /2 - 4 ) )
						? 0 : 1 ];
		} else {
                        ob->ob_owner = &nobjects[i >= (MAX_TARG / 2)];
		}

                ob->ob_clr = ob->ob_owner->ob_clr;
                ob->ob_symhgt = ob->ob_symwdt = 16;
                ob->ob_drawf = draw_target;
                ob->ob_movef = move_target;

                insert_x (ob, &topobj);
        }
}



void
create_explosion (SopwithObject *obop,
		  int      small)
{
	SopwithObject *ob, *obo;
	int      i, ic, speed;
	int      obox, oboy, obodx, obody, oboclr, obotype;
	gboolean     mansym;
	int      orient;


        obo = obop;
        obox   = obo->ob_x + ( obo->ob_symwdt >> 1 );
        oboy   = obo->ob_y + ( obo->ob_symhgt >> 1 );
        obodx  = obo->ob_dx >> 2;
        obody  = obo->ob_dy >> 2;
        oboclr = obo->ob_clr;

        if (((obotype = obo->ob_type) == TARGET)
                && (obo->ob_orient == 2)) {
                ic = 1;
                speed = gminspeed;
        } else {
                ic = small ? 6 : 2;
                speed = gminspeed >> ((explseed & 7) != 7);
        }

        mansym = (obotype == PLANE) && ((obo->ob_state == FLYING)
                                      || (obo->ob_state == WOUNDED));

        for (i = 1; i <= 15; i += ic) {
		ob = allocate_object ();
                if (ob == NULL) {
                        return;
		}

                ob->ob_type = EXPLOSION;

                set_velocity (ob, COS (i) * speed, SIN (i) * speed);
                ob->ob_dx += obodx;
                ob->ob_dy += obody;

                if ((explseed = (ob->ob_x = obox + ob->ob_dx)
		     * (ob->ob_y = oboy + ob->ob_dy)
		     * explseed + 7491) == 0) {
                        explseed = 74917777;
		}

                ob->ob_life = EXPLLIFE;
                orient = ob->ob_orient = (explseed & 0x01C0) >> 6;

                if (mansym && ((!orient) || (orient == 7))) {
                        mansym = orient = ob->ob_orient = 0;
                        ob->ob_dx = obodx;
                        ob->ob_dy = -gminspeed;
                }

                ob->ob_lx = ob->ob_ly = ob->ob_hitcount = ob->ob_speed = 0;
                ob->ob_owner = obo;
                ob->ob_clr = oboclr;
                ob->ob_symhgt = ob->ob_symwdt = 8;
                ob->ob_drawf = draw_explosion;
                ob->ob_movef = move_explosion;

                if (orient) {
#if 0 /* Play explosion sound */
                        initsound (ob, S_EXPLOSION);
#endif
		}

                insert_x (ob, obo);
        }
}



void
create_smoke (SopwithObject *obop)
{
	SopwithObject *ob, *obo;

	ob = allocate_object ();
        if (ob == NULL) {
                return;
	}

        ob->ob_type = SMOKE;

	obo = obop;

        ob->ob_x = obo->ob_x + 8;
        ob->ob_y = obo->ob_y - 8;
        ob->ob_dx = obo->ob_dx;
        ob->ob_dy = obo->ob_dy;
        ob->ob_lx = ob->ob_ly = ob->ob_ldx = ob->ob_ldy = 0;
        ob->ob_life = SMOKELIFE;
        ob->ob_owner = obo;
        ob->ob_symhgt = ob->ob_symwdt = 1;
        ob->ob_drawf = NULL;
        ob->ob_movef = move_smoke;
        ob->ob_clr = obo->ob_clr;
}




static int ifx[] = { MINFLCKX, MINFLCKX + 1000, MAXFLCKX - 1000, MAXFLCKX };
static int ify[] = { MAX_Y-1,  MAX_Y-1,         MAX_Y-1,         MAX_Y-1  };
static int ifdx[] = { 2,       2,               -2,              -2       };


static void
create_flocks (void)
{
	SopwithObject *ob;
	int     i, j;

        if (playmode == NOVICE) {
                return;
	}

        for (i = 0; i < MAX_FLCK; ++i) {

                if (! (ob = allocate_object ())) {
                        return;
		}

                ob->ob_type = FLOCK;
                ob->ob_state = FLYING;
                ob->ob_x = ifx[i];
                ob->ob_y = ify[i];
                ob->ob_dx = ifdx[i];
                ob->ob_dy = ob->ob_lx = ob->ob_ly = ob->ob_ldx = ob->ob_ldy = 0;
                ob->ob_orient = 0;
                ob->ob_life = FLOCKLIFE;
                ob->ob_owner = ob;
                ob->ob_symhgt = ob->ob_symwdt = 16;
                ob->ob_drawf = draw_flock;
                ob->ob_movef = move_flock;
                ob->ob_clr = 9;
                insert_x (ob, &topobj);
                for (j = 0; j < MAX_BIRD; j++) {
                        create_bird (ob, 1);
		}
        }
}


static int ibx[] = { 8, 3, 0, 6, 7, 14, 10, 12 };
static int iby[] = { 16, 1, 8, 3, 12, 10, 7, 14 };
static int ibdx[] = { -2, 2, -3, 3, -1, 1, 0, 0 };
static int ibdy[] = { -1, -2, -1, -2, -1, -2, -1, -2 };

void
create_bird (SopwithObject *obop,
	     int      i)
{
	SopwithObject *ob, *obo;

        if (!( ob = allocate_object ()))
                return;

        ob->ob_type = BIRD;

        ob->ob_x = (obo = obop)->ob_x + ibx[i];
        ob->ob_y = obo->ob_y - iby[i];
        ob->ob_dx = ibdx[i];
        ob->ob_dy = ibdy[i];
        ob->ob_orient = ob->ob_lx = ob->ob_ly = ob->ob_ldx = ob->ob_ldy = 0;
        ob->ob_life = BIRDLIFE;
        ob->ob_owner = obo;
        ob->ob_symhgt = 2;
        ob->ob_symwdt = 4;
        ob->ob_drawf = draw_bird;
        ob->ob_movef = move_bird;
        ob->ob_clr = obo->ob_clr;
        insert_x (ob, obo);
}



static void
create_oxen (void)
{
	SopwithObject *ob;
	int      i;
	int iox[] = {1376, 1608};
	int ioy[] = {80,   91};

        if (playmode == NOVICE) {
                for (i = 0; i < MAX_OXEN; ++i)
                        targets[MAX_TARG + i] = NULL;
                return;
        }

        for (i = 0; i < MAX_OXEN; ++i) {
                if (!(targets[MAX_TARG + i] = ob = allocate_object ())) {
                        return;
		}

                ob->ob_type = OX;
                ob->ob_state = STANDING;
                ob->ob_x = iox[i];
                ob->ob_y = ioy[i];
                ob->ob_orient = ob->ob_lx = ob->ob_ly = ob->ob_ldx = ob->ob_ldy
                              = ob->ob_dx = ob->ob_dy = 0;
                ob->ob_owner = ob;
                ob->ob_symhgt = 16;
                ob->ob_symwdt = 16;
                ob->ob_drawf = NULL;
                ob->ob_movef = move_ox;
                ob->ob_clr = 1;
                insert_x (ob, &topobj);
        }
}


static gboolean
clock_tick_handler (void *data)
{
#if 0
	if (!paused) {
#endif
		++counttick;
		
		/*--- DLC 96/12/27
		  ++movetick;
		  ----------------*/
		
		movetick+=10;
		
#if 0
		soundadj();
	}
#endif
	
	if (movetick > movemax) {
                movetick -= movemax;

		puts ("MOVE");

#if 0
                move_all_SopwithObject ();
                swgetjoy ();
                draw_everything ();
                swgetjoy ();
                resolve_collisions ();
                swgetjoy ();
		swsound ();
#endif
	}

	return TRUE;
}



void 
set_velocity (SopwithObject *obj,
	      int            xv,
	      int            yv)
{
	obj->ob_dx = xv >> 8;
	obj->ob_ldx = xv << 8;
	obj->ob_dy = yv >> 8;
	obj->ob_ldy = yv << 8;
}


