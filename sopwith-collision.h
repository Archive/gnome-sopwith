/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

#ifndef SOPWITH_COLLISION_H
#define SOPWITH_COLLISION_H

#include "sopwith-object.h"

void draw_score         (SopwithObject *obp);
void resolve_collisions (void);

void write_number       (int n,
			 int size);

void score_for_plane    (SopwithObject *ob);

#endif /* SOPWITH_COLLISION_H */
