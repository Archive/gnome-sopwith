/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
        swtitle  -      SW perform animation on the title screen

                        Copyright (C) 1984-2000 David L. Clark.

                        All rights reserved except as specified in the
                        file license.txt.  Distribution of this file
                        without the license.txt file accompanying is
                        prohibited.

                        Author: Dave Clark

        Modification History:
                        84-02-02        Development
                        87-03-10        Microsoft compiler.
                        87-03-11        Title reformatting.
                        87-04-01        Version 7.F15
                        96-12-27        New network version.
                        99-01-24        1999 copyright.
                        2000-10-29      Copyright update.
		        2000-12-25      Started GNOME port, see ChangeLog for 
 		                          further changes.
*/

#include <config.h>
#include "sopwith-title.h"

#include "sopwith-graphics.h"
#include "sw.h"


#include "sopwith-drawing-area.h"
#include <gdk-pixbuf/gdk-pixbuf.h>

extern  gboolean hires;                  /* High res debug flag    */
extern  gboolean titleflg;               /* Title flag             */
extern  int      tickmode;               /* Tick action to be done */
extern  int      counttick, countmove;   /* Performance counters   */
extern  int      movetick;               /* Move synchronization   */
extern  int      displx, disprx;         /* Display left and right bounds  */
extern  gboolean dispinit;               /* Initalized display flag.       */
extern  GRNDTYPE ground[];               /* Ground height by pixel         */


extern   char   swplnsym[][ANGLES][SYMBYTES];
extern   char   swtrgsym[][TARGBYTES];
extern   char   swoxsym[][OXBYTES];
extern   char   swhitsym[][SYMBYTES];
extern   char   swwinsym[][WINBYTES];

#include <stdio.h>

void
show_title (SopwithDrawingArea *drawing_area)
{
	GdkPixbuf *pixbuf;
	GdkPixmap *pixmap;
	GdkBitmap *mask;

	SopwithObject ob;
	int i, h;

        if (titleflg) {
                return;
	}

        tickmode = 1;

#if 0
        sound (S_TITLE, 0, NULL);
        swsound ();
#endif

/*---------------- Original BMB Version---------------

        set_text_color (3);
        position_text_cursor (13, 6);
        puts ("S O P W I T H");

        set_text_color (1);
        position_text_cursor (12, 8);
        puts ("(Version 7.F15)");

        set_text_color (3);
        position_text_cursor (5, 11);
        puts ("(c) Copyright 1984, 1985, 1987");

        set_text_color (1);
        position_text_cursor (6, 12);
        puts ("BMB ");
        set_text_color (3);
        puts ("Compuscience Canada Ltd.");

------------------ Original BMB Version---------------*/

/*---------------- New Network Version ---------------*/

        set_text_color (3);
        position_text_cursor (13, 4);
        puts ("S O P W I T H");

        set_text_color (1);
        position_text_cursor (9, 6);
        puts ("(Distribution Version)");

        set_text_color (3);
        position_text_cursor (5, 9);
        puts ("(c) Copyright 1984, 1985, 1987");

        set_text_color (1);
        position_text_cursor (6, 10);
        puts ("BMB ");
        set_text_color (3);
        puts ("Compuscience Canada Ltd.");

        set_text_color (3);
        position_text_cursor (1, 12);
        puts ("(c) Copyright 1984-2000 David L. Clark");

/*---------------- New Network Version-----------------*/

        use_on_screen_buffer ();

        displx = 700;
        dispinit = TRUE;
        draw_ground (drawing_area);

        ob.ob_type = PLANE;
        ob.ob_symhgt = ob.ob_symwdt = 16;
        ob.ob_clr = 1;
        ob.ob_newsym = swplnsym[0][0];

	pixbuf = gdk_pixbuf_new_from_file 
		("/gnome/share/pixmaps/sopwith/sopwith-player-1-plane-0-00.png");
	gdk_pixbuf_render_pixmap_and_mask (pixbuf,
					   &pixmap, &mask,
					   128);
	sopwith_drawing_area_draw_sprite (drawing_area,
					  260, 180,
					  pixmap, mask);
	gdk_pixmap_unref (pixmap);
	gdk_bitmap_unref (mask);
	gdk_pixbuf_unref (pixbuf);


        ob.ob_newsym = swwinsym[3];
        draw_sprite (50, 180, &ob);

	pixbuf = gdk_pixbuf_new_from_file 
		("/gnome/share/pixmaps/sopwith/sopwith-player-1-win-plane-3.png");
	gdk_pixbuf_render_pixmap_and_mask (pixbuf,
					   &pixmap, &mask,
					   128);
	sopwith_drawing_area_draw_sprite (drawing_area,
					  50, 180,
					  pixmap, mask);
	gdk_pixmap_unref (pixmap);
	gdk_bitmap_unref (mask);
	gdk_pixbuf_unref (pixbuf);


        ob.ob_type = OX;
        ob.ob_newsym = swoxsym[0];
        draw_sprite (100, ground[800] + 16, &ob);

	pixbuf = gdk_pixbuf_new_from_file 
		("/gnome/share/pixmaps/sopwith/sopwith-ox.png");
	gdk_pixbuf_render_pixmap_and_mask (pixbuf,
					   &pixmap, &mask,
					   128);
	sopwith_drawing_area_draw_sprite (drawing_area,
					  100, ground[800] + 16,
					  pixmap, mask);
	gdk_pixmap_unref (pixmap);
	gdk_bitmap_unref (mask);
	gdk_pixbuf_unref (pixbuf);



        ob.ob_type = TARGET;
        ob.ob_clr = 2;
        ob.ob_newsym = swtrgsym[3];
        draw_sprite (234, ground[934] + 16, &ob);


	pixbuf = gdk_pixbuf_new_from_file 
		("/gnome/share/pixmaps/sopwith/sopwith-player-2-tank.png");
	gdk_pixbuf_render_pixmap_and_mask (pixbuf,
					   &pixmap, &mask,
					   128);
	sopwith_drawing_area_draw_sprite (drawing_area,
					  234, ground[934] + 16,
					  pixmap, mask);
	gdk_pixmap_unref (pixmap);
	gdk_bitmap_unref (mask);
	gdk_pixbuf_unref (pixbuf);



        ob.ob_type = PLANE;
        ob.ob_newsym = swhitsym[0];
        draw_sprite (20, 160, &ob);

	pixbuf = gdk_pixbuf_new_from_file 
		("/gnome/share/pixmaps/sopwith/sopwith-player-2-falling-plane-0.png");
	gdk_pixbuf_render_pixmap_and_mask (pixbuf,
					   &pixmap, &mask,
					   128);
	sopwith_drawing_area_draw_sprite (drawing_area,
					  20, 160,
					  pixmap, mask);
	gdk_pixmap_unref (pixmap);
	gdk_bitmap_unref (mask);
	gdk_pixbuf_unref (pixbuf);


        ob.ob_type = SMOKE;
        ob.ob_symhgt = ob.ob_symwdt = 1;
        ob.ob_newsym = (char *)0x82;
        h = 150;
        for (i = 9; i; --i) {
		sopwith_drawing_area_draw_point (drawing_area,
						 30,
						 h +=5,
						 1);

                draw_sprite (30, h += 5, &ob);
	}
}



void
stop_playing_title_song (void)
{

        if (titleflg) {
                return;
	}

#if 0
        sound( 0, 0, NULL );
        swsound();
#endif
        tickmode = 0;
}



