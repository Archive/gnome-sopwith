/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

#ifndef SOPWITH_END_H
#define SOPWITH_END_H

#include "sopwith-object.h"

void sopwith_end (char          *msg,
		  gboolean       update);
void end_game    (int            targclr);

void winner      (SopwithObject *obp);
void loser       (SopwithObject *obp);

#endif /* SOPWITH_END_H */
