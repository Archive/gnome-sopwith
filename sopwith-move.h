/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

#ifndef SOPWITH_MOVE_H
#define SOPWITH_MOVE_H

#include "sopwith-object.h"

void move_all_objects  (void);

void insert_x          (SopwithObject *ob, 
		        SopwithObject *obp);
void delete_x          (SopwithObject *obp);

void crash_plane       (SopwithObject *obp);
void hit_plane         (SopwithObject *obp);

int  move_player_plane (SopwithObject *obp);
int  move_enemy_plane  (SopwithObject *obp);

int  move_shot         (SopwithObject *obp);
int  move_bomb         (SopwithObject *obp);
int  move_missile      (SopwithObject *obp);
int  move_starburst    (SopwithObject *obp);
int  move_target       (SopwithObject *obp);
int  move_explosion    (SopwithObject *obp);
int  move_smoke        (SopwithObject *obp);
int  move_flock        (SopwithObject *obp);
int  move_bird         (SopwithObject *obp);
int  move_ox           (SopwithObject *obp);

void move_object       (SopwithObject *ob,
			int           *x,
			int           *y);


#endif /* SOPWITH_MOVE_H */
