/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
        swcollsn -      SW collision resolution

                        Copyright (C) 1984-2000 David L. Clark.

                        All rights reserved except as specified in the
                        file license.txt.  Distribution of this file
                        without the license.txt file accompanying is
                        prohibited.

                        Author: Dave Clark

        Modification History:
                        84-02-02        Development
                        84-06-12        PCjr Speed-up
                        84-10-31        Atari
                        87-03-09        Microsoft compiler.
                        87-03-11        No explosion on bird-plane collision
                        87-03-12        Wounded airplanes.
                        87-03-12        More than 1 bullet to kill target.
                        87-03-13        Splatted bird symbol.
                        87-03-31        Missiles.
                        87-04-05        Missile and starburst support
		        2000-12-28      Started GNOME port, see ChangeLog for 
 		                          further changes.
*/

#include <config.h>
#include "sopwith-collision.h"

#include "sopwith-draw.h"
#include "sopwith-globals.h"
#include "sopwith-graphics.h"
#include "sopwith-init.h"
#include "sopwith-end.h"
#include "sopwith-move.h"
#include "sw.h"

#include <stdio.h>

extern  SopwithObject topobj, botobj;
extern  SopwithObject *nobjects;              /* SopwithObject list.                    */
extern  GRNDTYPE ground[];              /*  Ground height by pixel          */
extern  int     player;
extern  int     shothole;               /* Number of window shots to dislay */
extern  int     splatbird;              /* Number of splatted birds         */
extern  int     splatox;                /* Display a splatted OX            */
extern  int     numtarg[];              /*  Number of active targets        */
extern  MULTIO  *multbuff;
extern  int     gamenum;                /* Current game number              */
extern  int     endsts[];               /* End of game status and move count*/
extern  int     forcdisp;               /* Force display of ground          */
extern  int     gmaxspeed,gminspeed;    /* Speed range based on game number */
extern  int     counttick, countmove;   /* Performance counters            */

static  SopwithObject *killed[MAX_OBJS << 1],
                *killer[MAX_OBJS << 1];
static  int     killptr;

static  int     collsdx[MAX_PLYR];
static  int     collsdy[MAX_PLYR];
static  SopwithObject *collsno[MAX_PLYR];
static  int     collptr;
static  int     collxadj, collyadj;


static void     check_for_collision (SopwithObject *ob1, 
				     SopwithObject *ob2);
static void     check_for_crash     (SopwithObject *obp);
static void     kill_object         (SopwithObject *to_kill, 
				     SopwithObject *killer);
static void     dig_crater          (SopwithObject *ob);

static gboolean score_penalty       (int      ttype,
				     SopwithObject *ob,
				     int      score);
static void     score_for_target    (SopwithObject *obp,
				     int     score);


void
resolve_collisions (void)
{
	SopwithObject *ob, *obp, **obkd, **obkr;
	int      xmax, ymin, ymax, otype, i;
	int      prevx1, prevx2;
 
        collptr = killptr = 0;
        collxadj = 2;
        collyadj = 1;
	
        if (countmove & 1) {
                collxadj = -collxadj;
                collyadj = -collyadj;
        }

        use_off_screen_buffer ();

        prevx1 = topobj.ob_x;
        for (ob = topobj.ob_xnext; ob != &botobj; ob = ob->ob_xnext) {
                prevx2 = prevx1 = ob->ob_x;
		
                xmax = ob->ob_x + ob->ob_symwdt - 1;
                ymin = (ymax = ob->ob_y) - ob->ob_symhgt + 1;

                for (obp = ob->ob_xnext;
		     (obp != &botobj) && (obp->ob_x <= xmax);
		     obp = obp->ob_xnext) {
                        prevx2 = obp->ob_x;

                        if ((obp->ob_y >= ymin)
			    && ((obp->ob_y - obp->ob_symhgt + 1 ) <= ymax)) {
                                check_for_collision (ob, obp);
			}
                }
		
                if ((((otype = ob->ob_type) == PLANE)
		     && (ob->ob_state != FINISHED)
		     && (ob->ob_state != WAITING)
		     && (ob->ob_y < (ground[ob->ob_x + 8] + 24)))
		    || (((otype == BOMB) || (otype == MISSILE))
			&& (ob->ob_y < (ground[ob->ob_x + 4] + 12)))) {
                        check_for_crash (ob);
		}
        }

        obkd = killed;
        obkr = killer;
        for (i = 0; i < killptr; ++i, ++obkd, ++obkr) {
               kill_object (*obkd, *obkr);
	}

        obkd = collsno;
        for (i = 0; i < collptr; ++i, ++obkd) {
		ob = *obkd;
                ob->ob_dx = collsdx[i];
                ob->ob_dy = collsdy[i];
        }
}


static void
check_for_collision (SopwithObject *ob1, 
		     SopwithObject *ob2)
{
	SopwithObject *obt, *ob, *obp;
	int otype, ttype;

        ob = ob1;
        obp = ob2;
        otype = ob->ob_type;
        ttype = obp->ob_type;
        if (((otype == PLANE) && (ob->ob_state >= FINISHED))
	    || ((ttype == PLANE) && (obp->ob_state >= FINISHED))
	    || ((otype == EXPLOSION) && (ttype == EXPLOSION)))
                return;

        if (ob->ob_y < obp->ob_y) {
                obt = ob;
                ob = obp;
                obp = obt;
        }

        draw_sprite (15, 15, ob);
        if (draw_sprite_checking_collisions (obp->ob_x - ob->ob_x + 15,
					     obp->ob_y - ob->ob_y + 15,
					     obp)) {
                if (killptr < ((MAX_OBJS << 1) - 1)) {
                        killed[killptr] = ob;
                        killer[killptr++] = obp;
                        killed[killptr] = obp;
                        killer[killptr++] = ob;
                }
	}

        clear_collision_detection_area ();
}



static void
check_for_crash (SopwithObject *obp)
{
	SopwithObject *ob;
	int x, xmax, y;
	gboolean hit = FALSE;

        ob = obp;
	
	draw_sprite (15, 15, ob);

        xmax = ob->ob_x + ob->ob_symwdt - 1;

        for (x = ob->ob_x; x <= xmax; ++x) {
                if ((y = (int) ground[x] - ob->ob_y + 15) > 15) {
                        hit = TRUE;
                        break;
                }
                if (y < 0) {
                        continue;
		}

		hit = draw_point_checking_collisions (x - ob->ob_x + 15, y, 0x80);

                if (hit) {
                        break;
		}
        }
	
        clear_collision_detection_area ();

        if ((hit) && (killptr < (MAX_OBJS << 1))) {
                killed[killptr] = ob;
                killer[killptr++] = NULL;
        }
}



static void
kill_object (SopwithObject *to_kill, 
	     SopwithObject *killer)
{
	SopwithObject *ob, *obt;
	int state, ttype, i;

        ob = to_kill;
        obt = killer;
        ttype = obt ? obt->ob_type : GROUND;
	
	if (((ttype == BIRD) || (ttype == FLOCK))
	    && (ob->ob_type != PLANE)) {
                return;
	}
	
	switch (ob->ob_type) {
	case BOMB:
	case MISSILE:
		create_explosion (ob, 0);
		ob->ob_life = -1;
		if (!obt) {
			dig_crater (ob);
		}
#if 0
		stopsound (ob);
#endif
		return;
		
	case SHOT:
		ob->ob_life = 1;
		return;
		
	case STARBURST:
		if ((ttype == MISSILE) || (ttype == BOMB) || !obt)
			ob->ob_life = 1;
		return;
		
	case EXPLOSION:
		if (!obt) {
			ob->ob_life = 1;
#if 0
			stopsound (ob);
#endif
		}
		return;
		
	case TARGET:
		if (ob->ob_state != STANDING) {
			return;
		}
		if ((ttype == EXPLOSION) || (ttype == STARBURST)) {
			return;
		}
		
		if ((ttype == SHOT)
		    && ((ob->ob_hitcount += TARGHITCOUNT)
			<= (TARGHITCOUNT * (gamenum + 1)))) {
			return;
		}
		
		ob->ob_state = FINISHED;
		create_explosion (ob, 0);
		
		use_on_screen_buffer ();
		draw_map_object (ob);
		use_off_screen_buffer ();
		
		score_for_target (ob, (ob->ob_orient == 2 ) ? 200 : 100);
		if (!--numtarg[ob->ob_clr - 1]) {
			end_game (ob->ob_clr);
		}
		return;

	case PLANE:
		if (((state = ob->ob_state) == CRASHED)
		    || (state == GHOSTCRASHED)) {
			return;
		}
		
		if (endsts[ob->ob_index] == WINNER) {
			return;
		}
		
		if ((ttype == STARBURST)
		    || ((ttype == BIRD) && ob->ob_athome)) {
			return;
		}
		
		if (!obt) {
			if (state == FALLING) {
#if 0
				stopsound (ob);
#endif
				create_explosion (ob, 1);
				dig_crater (ob);
			}  else {
				if (state < FINISHED) {
					score_for_plane (ob);
					create_explosion (ob, 1);
					dig_crater (ob);
				}
			}
			
			crash_plane (ob);
			return;
		}
		
		if (state >= FINISHED) {
			return;
		}

		if (state == FALLING) {
			if (ob->ob_index == player) {
				if (ttype == SHOT) {
					++shothole;
				} else if ((ttype == BIRD)
					   || (ttype == FLOCK)) {
					++splatbird;
				}
			}
			return;
		}
		
		if ((ttype == SHOT) || (ttype == BIRD)
		     || (ttype == OX) || (ttype == FLOCK)) {
			if (ob->ob_index == player) {
				if (ttype == SHOT) {
					++shothole;
				} else if (ttype == OX) {
					++splatox;
				} else {
					++splatbird;
				}
			}
			if (state == FLYING) {
				ob->ob_state = WOUNDED;
				return;
			}
			if ( state == STALLED ) {
				ob->ob_state = WOUNDSTALL;
				return;
			}
		} else {
			create_explosion (ob, 1);
			if (ttype == PLANE) {
				collsdx[collptr]
					= ((ob->ob_dx + obt->ob_dx) >> 1)
					+ (collxadj = -collxadj);
				collsdy[collptr]
					= ((ob->ob_dy + obt->ob_dy) >> 1)
					+ (collyadj = -collyadj);
				collsno[collptr++] = ob;
			}
		}
		
		hit_plane (ob);
		score_for_plane (ob);
		return;
		
	case BIRD:
		ob->ob_life = score_penalty (ttype, obt, 25) ? -1 : -2;
		return;
		
	case FLOCK:
		if ((ttype != FLOCK) && (ttype != BIRD)
		    && (ob->ob_state == FLYING)) {
			for (i = 0; i < 8; ++i) {
				create_bird (ob, i);
			}
			
			ob->ob_life = -1;
			ob->ob_state = FINISHED;
		}
		return;
		
	case OX:
		if (ob->ob_state != STANDING) {
			return;
		}

		if ((ttype == EXPLOSION) || (ttype == STARBURST)) {
			return;
		}

		score_penalty (ttype, obt, 200);
		ob->ob_state = FINISHED;
		return;
	case GROUND:
	case SMOKE:
	case DUMMYTYPE:
        }
}



static gboolean
score_penalty (int      ttype,
	       SopwithObject *ob,
	       int      score)
{
	register SopwithObject *obt;
	
        obt = ob;

        if ((ttype == SHOT) || (ttype == BOMB) || (ttype ==  MISSILE)
	     || ((ttype == PLANE)
		  && ((obt->ob_state == FLYING)
		       || (obt->ob_state == WOUNDED)
		       || ((obt->ob_state == FALLING)
			    && (obt->ob_hitcount == FALLCOUNT)))
		  && (!obt->ob_athome))) {
                score_for_target (obt, score);
                return TRUE;
        }
        return FALSE;
}




static void
score_for_target (SopwithObject *obp,
		  int     score)
{
	register SopwithObject *ob;
	
        ob = obp;
        
	if (((playmode != MULTIPLE) && (playmode != ASYNCH))
	     || (multbuff->mu_maxplyr == 1)) {
                if (ob->ob_clr == 1) {
                        nobjects[0].ob_score -= score;
                } else {
                        nobjects[0].ob_score += score;
		}
                draw_score (&nobjects[0]);
        } else {
                nobjects[2 - ob->ob_clr].ob_score += score;
                draw_score (&nobjects[2 - ob->ob_clr]);
        }
}




void
score_for_plane (SopwithObject *ob)
{
        score_for_target (ob, 50);
}





void
write_number (int n,
	      int size)
{
	int  i = 0;
	int  d, t;
	gboolean first = TRUE;
	
        if (n < 0) {
                n = -n;
                putchar ('-');
                ++i;
        }

        for (t = 10000; t > 1; n %= t, t /= 10) {
                if ((d = n / t) || (!first)){
                        first = FALSE;
                        putchar (d + '0');
                        ++i;
                }
	}

        putchar (n + '0');

        ++i;
        while (++i <= size) {
                putchar (' ');
	}
}




void
draw_score (SopwithObject *obp)
{
        SopwithObject *ob;
	
        position_text_cursor (((ob = obp )->ob_clr - 1) * 7 + 2, 24);
        set_text_color (ob->ob_clr);
        write_number (ob->ob_score, 6);
}


static int crater_depth[8] = { 1, 2, 2, 3, 3, 2, 2, 1 };

static void
dig_crater (SopwithObject *ob)
{
	register int    i, x, y, ymin, ymax;
	int             xmin, xmax;
	
        xmin = ob->ob_x + (ob->ob_symwdt - 8) / 2;
        xmax = xmin + 7;
	
        for (x = xmin, i = 0; x <= xmax; ++x, ++i) {
                ymax = ground[x];
                if ((y = orground[x] - 20) < 20)
                        y = 20;
                if ((ymin = ymax - crater_depth[i] + 1) <= y)
                        ymin = y + 1;
                ground[x] = ymin - 1;
        }
        forcdisp = TRUE;
}

