/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
        swdispc  -      Display all players and SopwithObject

                        Copyright (C) 1984-2000 David L. Clark.

                        All rights reserved except as specified in the
                        file license.txt.  Distribution of this file
                        without the license.txt file accompanying is
                        prohibited.

                        Author: Dave Clark

        Modification History:
                        84-02-21        Development
                        84-06-12        PCjr Speed-up
                        87-03-09        Microsoft compiler.
                        87-03-12        Wounded airplanes.
                        87-03-13        Splatted bird symbol.
                        87-04-05        Missile and starburst support
			2000-12-29      Started GNOME porting. See
                                          ChangeLog for further changes.  
*/

#include <config.h>
#include "sopwith-draw.h"

#include "sopwith-graphics.h"
#include "sw.h"


extern  OLDWDISP wdisp[];               /*  World display status            */
extern  SopwithObject *nobjects;        /*  SopwithObject list.                   */
extern  SopwithObject *objtop;          /*  Start of object list.           */
extern  int     shothole;               /*  Number of shot holes to display */
extern  int     splatbird;              /*  Number of splatted birds        */
extern  int     splatox;                /* Display splatted ox              */
extern  int     oxsplatted;             /* An ox has been splatted          */
extern  char    swshtsym[];             /*  Shot hole symbol                */
extern  char    swsplsym[];             /*  Splatted bird symbol            */
extern  int     countmove;              /*  Move number                     */


static void play_plane_sound   (SopwithObject *obp);
static void rand_seed          (void);
static void draw_shot_window   (void);
static void draw_splatted_bird (void);


void
draw_player_plane (SopwithObject *ob)
{
        if (shothole) {
                draw_shot_window ();
	}
	if (splatbird) {
                draw_splatted_bird ();
	}
        play_plane_sound (ob);
}





void 
draw_bomb (SopwithObject *obp)
{
	SopwithObject *ob;

        if ((ob = obp)->ob_dy <= 0) {
#if 0
                sound (S_BOMB, -(ob->ob_y), ob);
#endif
	}
}



void
draw_missile (SopwithObject *obp)
{
}


void
draw_starburst (SopwithObject *obp)
{
}


void
draw_explosion (SopwithObject *obp)
{
	SopwithObject *ob;

        if ((ob = obp)->ob_orient) {
#if 0
                sound (S_EXPLOSION, ob->ob_hitcount, ob);
#endif
	}
}



void
draw_enemy_plane (SopwithObject *ob)
{
        play_plane_sound (ob);
}



void
draw_other_player_plane (SopwithObject *ob)
{
        play_plane_sound (ob);
}



void
draw_target (SopwithObject *ob)
{
        if (ob->ob_firing) {
#if 0
                sound (S_SHOT, 0, ob);
#endif
	}
}


void
draw_flock (SopwithObject *ob)
{
}



void
draw_bird (SopwithObject *ob)
{
}





static void
play_plane_sound (SopwithObject *obp)
{
	SopwithObject *ob;
	
        ob = obp;
        if (ob->ob_firing) {
#if 0
                sound (S_SHOT, 0, ob);
#endif
	} else {
                switch ( ob->ob_state ) {
		case FALLING:
			if (ob->ob_dy >= 0) {
#if 0
				sound (S_HIT, 0, ob);
#endif
			} else {
#if 0
				sound (S_FALLING, ob->ob_y, ob);
#endif
                                break;
			}
		case FLYING:
#if 0
			sound (S_PLANE, -(ob->ob_speed), ob);
#endif
			break;
			
		case STALLED:
		case WOUNDED:
		case WOUNDSTALL:
#if 0
			sound (S_HIT, 0, ob);
#endif
			break;
		default:
			break;
                }
	}
}



void
draw_map_object (SopwithObject *obp)
{
        SopwithObject  *ob;
        OLDWDISP *ow;
	int       oldplot;

        ob = obp;
        ow = &wdisp[ob->ob_index];

        if (ow->ow_xorplot) {
                draw_point (ow->ow_x, ow->ow_y, ow->ow_xorplot - 1);
	}

        if (ob->ob_state >= FINISHED) {
                ow->ow_xorplot = 0;
        } else {
                oldplot = draw_point_checking_collisions (ow->ow_x = SCR_CENTR
							  + (ob->ob_x + ob->ob_symwdt / 2)
							  / WRLD_RSX,
							  ow->ow_y
							  = (ob->ob_y - ob->ob_symhgt / 2)
							  / WRLD_RSY,
							  ob->ob_owner->ob_clr);

                if ((oldplot == 0) || ((oldplot & 0x0003) == 3)) {
		        ow->ow_xorplot = oldplot + 1;
                        return;
                }
                draw_point (ow->ow_x, ow->ow_y, oldplot);
                ow->ow_xorplot = 0;
        }
}


static unsigned long seed = 74917777;

static void 
rand_seed (void)
{
	seed = seed * countmove + 7491;

        if (seed == 0) {
                seed = 74917777;
	}
}



static void
draw_shot_window (void)
{
	SopwithObject ob;

        ob.ob_type = DUMMYTYPE;
        ob.ob_symhgt = ob.ob_symwdt = 16;
        ob.ob_clr = 1;
        ob.ob_newsym = swshtsym;
        do {
                rand_seed ();
                draw_sprite ((unsigned) (seed % (SCR_WDTH - 16)),
			     (unsigned) (seed % (SCR_HGHT - 50)) + 50,
			     &ob);
        } while (--shothole);
}


static void
draw_splatted_bird (void)
{
	SopwithObject ob;

        ob.ob_type = DUMMYTYPE;
        ob.ob_symhgt = ob.ob_symwdt = 32;
        ob.ob_clr = 2;
        ob.ob_newsym = swsplsym;
        do {
                rand_seed ();
                draw_sprite ((unsigned) (seed % (SCR_WDTH - 32)),
			     (unsigned) (seed % (SCR_HGHT - 60)) + 60,
			     &ob);
        } while (--splatbird);
}


void
draw_splatted_ox (void)
{
	SopwithObject *ob;
	int     i;

#if 0 /* what the hell is this code doing? */
        swsetblk (0, SCR_SEGM,
                  ((SCR_HGHT - SCR_MNSH - 2) >> 1) * SCR_LINW, 0xAA);
        swsetblk (SCR_ROFF, SCR_SEGM,
                  ((SCR_HGHT - SCR_MNSH - 3) >> 1) * SCR_LINW, 0xAA);
#endif
        splatox = 0;
        oxsplatted = 1;

        ob = nobjects;
        for (i = 0; i < MAX_OBJS; ++i, ob++) {
                ob->ob_drwflg = ob->ob_delflg = 0;
	}
}
