/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*

        swgrph   -      SW screen graphics

                        Copyright (C) 1984-2000 David L. Clark.

                        All rights reserved except as specified in the
                        file license.txt.  Distribution of this file
                        without the license.txt file accompanying is
                        prohibited.

                        Author: Dave Clark

        Modification History:
                        84-02-21        Development
                        84-06-13        PCjr Speed-up
                        85-11-05        Atari
                        87-03-09        Microsoft compiler.
			2000-12-28      Started GNOME porting. See
                                          ChangeLog for further changes.  
*/


#include <config.h>
#include "sopwith-graphics.h"

#include "sopwith-drawing-area.h"

#include "sw.h"

#include <string.h>

extern  int     displx, disprx;         /* Display left and right bounds    */
extern  int     dispdx;                 /* Display shift                    */
extern  char    auxdisp[];              /* Auxiliary display area           */
extern  GRNDTYPE ground[];              /* Ground height by pixel           */
extern  gboolean dispinit;              /* Initalized display flag.         */
extern  SopwithObject *objtop;                /* Top of object list               */
extern  SopwithObject *deltop;                /* Newly deallocated SopwithObject list   */
extern  int     forcdisp;               /* Force display of ground          */
extern  long    trap14();               /* BIOS trap                        */

static  GRNDTYPE grndsave[SCR_WDTH];    /* Saved ground buffer for last     */
                                        /*   last display                   */

static void draw_sprite_color (SopwithObject *ob,
			       int      x, 
			       int      y,
			       char    *symbol,
			       int      clr, 
			       int     *retcode);

static void draw_point_color  (int  x, 
			       int  y, 
			       int  color, 
			       int *old_color);

#if 0
static  int     palette[] = {   /* Colour palette                           */
        0x000,                  /*   0 = black    background                */
        0x037,                  /*   1 = blue     planes,targets,explosions */
        0x700,                  /*   2 = red      planes,targets,explosions */
        0x777,                  /*   3 = white    bullets                   */
        0x000,                  /*   4                                      */
        0x000,                  /*   5                                      */
        0x000,                  /*   6                                      */
        0x070,                  /*   7 = green    ground                    */
        0x000,                  /*   8                                      */
        0x433,                  /*   9 = tan      oxen, birds               */
        0x420,                  /*  10 = brown    oxen                      */
        0x320,                  /*  11 = brown    bottom of ground display  */
        0x000,                  /*  12                                      */
        0x000,                  /*  13                                      */
        0x000,                  /*  14                                      */
        0x000                   /*  15                                      */
};
#endif

#if 0
static  char    spcbirds[BIRDSYMS][BRDBYTES*2];   /* Special bird symbol    */
                                                  /* colour video maps      */
#endif



/*---------------------------------------------------------------------------

        Main display loop.   Delete and display all visible SopwithObject.
        Delete any newly deleted SopwithObject

---------------------------------------------------------------------------*/


void
draw_everything (void)
{
        register SopwithObject *ob;

        use_on_screen_buffer ();

        for (ob = objtop; ob; ob = ob->ob_next) {
                if ((!( ob->ob_delflg && ob->ob_drwflg))
                        || (ob->ob_symhgt == 1)
                        || (ob->ob_oldsym != ob->ob_newsym)
                        || (ob->ob_y != ob->ob_oldy )
                        || ((ob->ob_oldx + displx) != ob->ob_x)) {
                        if (ob->ob_delflg)
                                draw_sprite_color (ob, ob->ob_oldx, ob->ob_oldy,
						   ob->ob_oldsym, ob->ob_clr, NULL);
                        if (!ob->ob_drwflg)
                                continue;
                        if ((ob->ob_x < displx) || (ob->ob_x > disprx)) {
                                ob->ob_drwflg = 0;
                                continue;
                        }

                        draw_sprite_color (ob, ob->ob_oldx = ob->ob_x - displx,
					   ob->ob_oldy = ob->ob_y,
					   ob->ob_newsym,
					   ob->ob_clr, NULL);
                }

                if (ob->ob_drawf) {
                        (*(ob->ob_drawf)) (ob);
		}
        }

        for (ob = deltop; ob; ob = ob->ob_next) {
                if (ob->ob_delflg) {
                        draw_sprite_color (ob, ob->ob_oldx, ob->ob_oldy,
					   ob->ob_oldsym, ob->ob_clr, NULL);
		}
	}
#if 0
        draw_ground ();
#endif
        dispinit = FALSE;
        forcdisp = FALSE;
}





/*---------------------------------------------------------------------------

        Update display of ground.   Delete previous display of ground by
        XOR graphics.

---------------------------------------------------------------------------*/


static void
draw_ground_color (SopwithDrawingArea *drawing_area,
		   GRNDTYPE           *gptr)
{
	GRNDTYPE prev_y, y;
	int       gmask, x;

	prev_y = gptr[0];

        for (x = 0; x < SCR_WDTH; x++) {
		y = gptr[x];

                if (prev_y == y) {
			sopwith_drawing_area_draw_point (drawing_area,
							 x, y,
							 3);
		} else if (prev_y < y) {
                        do  {
				sopwith_drawing_area_draw_point (drawing_area,
								 x, prev_y,
								 3);
                        } while (++prev_y < y);
                } else {
                        do  {
				sopwith_drawing_area_draw_point (drawing_area,
								 x, prev_y,
								 3);
                        } while (--prev_y > y);
		}
        }
}



void
draw_ground (SopwithDrawingArea *drawing_area)
{
        if (!dispinit) {
	        if (!(dispdx || forcdisp)) {
                        return;
		}
                draw_ground_color (drawing_area, grndsave);
        }

        memmove (grndsave, ground + displx, SCR_WDTH * sizeof (GRNDTYPE));

        draw_ground_color (drawing_area, ground + displx);
}







/*---------------------------------------------------------------------------

        Clear the collision detection portion of auxiliary video ram

---------------------------------------------------------------------------*/



void
clear_collision_detection_area (void)
{
#if 0
	long   *sptr;
	int    l;

        sptr = (long *) (dispoff + (SCR_HGHT - 1) * 160);
        if (scrtype == 2) {
                for (l = 32; l; --l) {
                        *sptr = *(sptr + 1) = *(sptr + 2) = 0L;
                        sptr -= 20;
                }
	} else {
                for ( l = 16; l; --l ) {
                        *sptr = *(sptr + 1) = *(sptr + 2) = *(sptr + 3)
                              = *(sptr + 4) = *(sptr + 5) = 0L;
                        sptr -= 40;
                }
	}
#endif
}



/*---------------------------------------------------------------------------

        Display an object's current symbol at a specified screen location
        Collision detection may or may not be asked for.

        Different routines are used to display symbols on colour or
        monochrome systems.

---------------------------------------------------------------------------*/


void
draw_sprite (int      x, 
	     int      y, 
	     SopwithObject *ob)
{
	draw_sprite_color (ob, x, y, ob->ob_newsym, ob->ob_clr, NULL);

	/*--------------------------------

;-----------------------------------------------------------------------------

drawsym:
        push    BX                      ; save object pointer

        push    BX                      ; set up mask for xor color adj.
        mov     BX,AX                   ;
        and     BX,3                    ;
        mov     AL,CS:cmsktab[BX]       ;
        mov     colmask,AL              ;
        pop     BX                      ;

        call    setup                   ; setup display parameters
        jnz     ds1                     ; symbol is a point?
        call    drawpnt                 ;    YES - draw point
        jmp     dsret                   ;          return

;       plot a rastor line of a symbol to the video ram.  Register
;       values at this time are
;               AL - byte count of segment display
;               AH - byte count of horizontal wraparound
;               BX - offset from end of symbol line to beginning of next
;               CL - number of bits to shift each byte
;               BP - number of lines to display
;               SI - pointer to first symbol byte to display
;               DI - pointer to first video ram byte to use

ds1:
        push    AX                      ; save segment lengths AL
        push    DI                      ; save current video ram location
        xor     CH,CH                   ; reset byte rotation hold areas
        push    BX                      ; save line to line adjustment
        lea     BX,CS:trans             ; address of byte translation table
        call    dispds                  ; display segment

        cmp     AH,0                    ; wrap around segment ?
        jl      ds2                     ;  NO-   goto normal carry processing
        mov     AL,AH                   ;  YES-  add wrap segment length
        xor     AH,AH                   ;        to symbol address SI
        add     SI,AX
        jmp     ds3                     ;        jump carry

ds2:
        cmp     CH,0                    ; shift carry present
        jz      ds3                     ;  NO -goto loop increment
        mov     DH,CH                   ; plot carry into ram using
        call    adjcolor                ; color adjustment
        xor     ES:[DI],DH              ;
ds3:
        pop     BX                      ; restore line to line adjustment
        pop     DI                      ; restore old start location
        add     DI,BX                   ; destination to start of new line
        xchg    BX,adjrast              ; exchange line to line offsets
        pop     AX                      ; restore segment lengths
        dec     BP                      ; decrement line counter
        jnz     ds1                     ; loop until line counter is 0

dsret:
        pop     BX                      ; restore registers
        ret



;       plot a rastor line segment to the video ram.  Register
;       values at this time are
;               AL - byte count of segment
;               AH - ---don't care---
;               BX - rastor line offset
;               CL - number of bits to shift each byte
;               CH - carry from previous shift
;               DX - screen line length in bytes
;               SI - pointer to first symbol byte to display
;               DI - pointer to first video ram byte to use

dispds:
        xor     DL,DL                   ; clear shift carry area
        mov     DH,[SI]                 ; symbol character
        shr     DX,CL                   ; shift right CL bits
        or      DH,CH                   ; or carry from previous shift

        call    adjcolor                ; adjust symbol for color of object

        xor     ES:[DI],DH              ; plot to video ram
        mov     CH,DL                   ; save carry
        inc     SI                      ; increment symbol pointer
        inc     DI                      ; video ram pointer
        dec     AL                      ; decrement byte counter
        jnz     dispds                  ; loop until segment done

        ret

	--------------------------------*/
}



int
draw_sprite_checking_collisions (int      x, 
				 int      y,
				 SopwithObject *ob)
{
	int retcode = FALSE;

#if 0
	draw_sprite_color (ob, x, y, ob->ob_newsym, ob->ob_clr, &retcode);
#endif
        return retcode;
}




char    fill[] = {
0x00,0x03,0x03,0x03,0x0C,0x0F,0x0F,0x0F,0x0C,0x0F,0x0F,0x0F,0x0C,0x0F,0x0F,0x0F,
0x30,0x33,0x33,0x33,0x3C,0x3F,0x3F,0x3F,0x3C,0x3F,0x3F,0x3F,0x3C,0x3F,0x3F,0x3F,
0x30,0x33,0x33,0x33,0x3C,0x3F,0x3F,0x3F,0x3C,0x3F,0x3F,0x3F,0x3C,0x3F,0x3F,0x3F,
0x30,0x33,0x33,0x33,0x3C,0x3F,0x3F,0x3F,0x3C,0x3F,0x3F,0x3F,0x3C,0x3F,0x3F,0x3F,
0xC0,0xC3,0xC3,0xC3,0xCC,0xCF,0xCF,0xCF,0xCC,0xCF,0xCF,0xCF,0xCC,0xCF,0xCF,0xCF,
0xF0,0xF3,0xF3,0xF3,0xFC,0xFF,0xFF,0xFF,0xFC,0xFF,0xFF,0xFF,0xFC,0xFF,0xFF,0xFF,
0xF0,0xF3,0xF3,0xF3,0xFC,0xFF,0xFF,0xFF,0xFC,0xFF,0xFF,0xFF,0xFC,0xFF,0xFF,0xFF,
0xF0,0xF3,0xF3,0xF3,0xFC,0xFF,0xFF,0xFF,0xFC,0xFF,0xFF,0xFF,0xFC,0xFF,0xFF,0xFF,
0xC0,0xC3,0xC3,0xC3,0xCC,0xCF,0xCF,0xCF,0xCC,0xCF,0xCF,0xCF,0xCC,0xCF,0xCF,0xCF,
0xF0,0xF3,0xF3,0xF3,0xFC,0xFF,0xFF,0xFF,0xFC,0xFF,0xFF,0xFF,0xFC,0xFF,0xFF,0xFF,
0xF0,0xF3,0xF3,0xF3,0xFC,0xFF,0xFF,0xFF,0xFC,0xFF,0xFF,0xFF,0xFC,0xFF,0xFF,0xFF,
0xF0,0xF3,0xF3,0xF3,0xFC,0xFF,0xFF,0xFF,0xFC,0xFF,0xFF,0xFF,0xFC,0xFF,0xFF,0xFF,
0xC0,0xC3,0xC3,0xC3,0xCC,0xCF,0xCF,0xCF,0xCC,0xCF,0xCF,0xCF,0xCC,0xCF,0xCF,0xCF,
0xF0,0xF3,0xF3,0xF3,0xFC,0xFF,0xFF,0xFF,0xFC,0xFF,0xFF,0xFF,0xFC,0xFF,0xFF,0xFF,
0xF0,0xF3,0xF3,0xF3,0xFC,0xFF,0xFF,0xFF,0xFC,0xFF,0xFF,0xFF,0xFC,0xFF,0xFF,0xFF,
0xF0,0xF3,0xF3,0xF3,0xFC,0xFF,0xFF,0xFF,0xFC,0xFF,0xFF,0xFF,0xFC,0xFF,0xFF,0xFF
};


extern  char    swbrdsym[BIRDSYMS][BRDBYTES];

static void
draw_sprite_color (SopwithObject *ob,
		   int            x, 
		   int            y,
		   char          *symbol,
		   int            clr, 
		   int           *retcode)
{
#if 0
	char   *s, *sptr, *sym;
	int     j, c1, c2, c;
	int     rotr, rotl, wdth, wrap, n;
	int     cr, pc1, pc2, invert, enhance1;

        if (!(sym = symbol)) {
                return;
	}

        if ((ob->ob_symhgt == 1) && (ob->ob_symwdt == 1)) {
                draw_point_color (x, y, (int) sym, retcode);
                return;
        }

        rotr = x & 0x0007;
        rotl = 8 - rotr;

        if ((wrap = (wdth = ob->ob_symwdt >> 2)
	     - (n = SCR_LINW - (x >> 2))) > 0) {
                wdth = n;
	}

        if ((n = ob->ob_symhgt) > (y + 1)) {
                n = y + 1;
		sptr = dispoff + (( SCR_HGHT - y - 1) * 160)
			+ ((x & 0xFFF0) >> 1)
			+ ((x & 0x0008) >> 3);
	}

        invert = (clr & 0x0003) == 2 ? -1 : 0;
        enhance1 = (((j = ob->ob_type) == FLOCK) || (j == BIRD)
		    || (j == OX)) ? -1 : 0;
        if (j == BIRD) {
                sym = (char *) spcbirds + ((sym - (char *) swbrdsym) << 1);
	}

        while (n--) {
                s = sptr;
                j = wdth;
                pc1 = pc2 = 0;
                while (j--) {
                        if (j) {
                                c = 0xFF;
                                --j;
                        } else {
                                c = 0xF0;
			}

                        cr = (c1 = *sym++ & c) << rotl;
                        c1 = (c1 >> rotr) | pc1;
                        pc1 = cr;
                        cr = (c2 = *sym++ & c) << rotl;
                        c2 = (c2 >> rotr) | pc2;
                        pc2 = cr;
                        c = c1 | c2;

                        if (retcode
			    && (c & (*s | *(s+2)) & 0xFF)) {
                                *retcode = TRUE;
                                retcode = 0;
                        }

                        *s     ^= c1 ^ (c & invert);
                        *(s+2) ^= c2 ^ (c & invert);
                        *(s+6) ^= c & enhance1;

                        if ((long) s & 1) {
                                s += 7;
                        } else {
                                ++s;
			}
                }
                if (wrap >= 0) {
                        sym += wrap & 0xFFFE;
                } else {
                        c = pc1 | pc2;
                        if (retcode
			    && (c & (*s | *(s+2)) & 0xFF)) {
                                *retcode = TRUE;
                                retcode = 0;
                        }
                        *s     ^= pc1 ^ (c & invert);
                        *(s+2) ^= pc2 ^ (c & invert);
                        *(s+6) ^= c & enhance1;
                }
                sptr += 160;
        }
#endif
}



/*---------------------------------------------------------------------------

        External calls to display a point of a specified colour at a
        specified position.   The point request may or may not ask for
        collision detection by returning the old colour of the point.

        Different routines are used to display points on colour or
        monochrome systems.

---------------------------------------------------------------------------*/



void
draw_point (int x, 
	    int y, 
	    int color)
{
        draw_point_color (x, y, color, NULL);
}


int
draw_point_checking_collisions (int x, 
				int y, 
				int color)
{
	int old_color = 0;
#if 0
        draw_point_color (x, y, color, &old_color);
#endif
        return old_color;
}



static void
draw_point_color (int  x, 
		  int  y, 
		  int  color, 
		  int *old_color)
{
#if 0
	int c, mask;
	char *sptr;

        sptr = dispoff + ((SCR_HGHT - y - 1) * 160)
                       + ((x & 0xFFF0) >> 1)
                       + ((x & 0x0008) >> 3);
        mask = 0x80 >> (x &= 0x0007);

        if (old_color) {
                c = (*sptr & mask)
                        | ((*(sptr + 2) & mask) << 1)
                        | ((*(sptr + 4) & mask) << 2)
                        | ((*(sptr + 6) & mask) << 3);
                *old_color = (c >> (7 - x)) & 0x00FF;
        }

        c = color << (7 - x);
        if (color & 0x0080) {
                *sptr       ^= (mask & c);
                *(sptr + 2) ^= (mask & (c >> 1));
                *(sptr + 4) ^= (mask & (c >> 2));
                *(sptr + 6) ^= (mask & (c >> 3));
        } else {
                mask = ~mask;
                *sptr       &= mask;
                *(sptr + 2) &= mask;
                *(sptr + 4) &= mask;
                *(sptr + 6) &= mask;

                mask = ~mask;
                *sptr |= (mask & c);
                *(sptr+2) |= (mask & (c >> 1));
                *(sptr+4) |= (mask & (c >> 2));
                *(sptr+6) |= (mask & (c >> 3));
        }
#endif
}

#if 0

static void
copy (char *from, 
      char *to)
{
	int i;

        for (i = 4; i; --i) {
                *to++ = *from++;
                *to++ = '\0';
        }
}



static void
invert (char *symbol,
	int   bytes)
{
	int c1, c2;
	char *s;
	int n;

        s = symbol;
        for (n = bytes >> 1; n; --n) {
                c1 = *s;
                c2 = *(s + 1);
                *s++ =  ((c1 << 1) & 0x80)
                        | ((c1 << 2) & 0x40)
                        | ((c1 << 3) & 0x20)
                        | ((c1 << 4) & 0x10)
                        | ((c2 >> 3) & 0x08)
                        | ((c2 >> 2) & 0x04)
                        | ((c2 >> 1) & 0x02)
                        | (c2 & 0x01);
                *s++ =  (c1 & 0x80)
                        | ((c1 << 1) & 0x40)
                        | ((c1 << 2) & 0x20)
                        | ((c1 << 3) & 0x10)
                        | ((c2 >> 4) & 0x08)
                        | ((c2 >> 3) & 0x04)
                        | ((c2 >> 2) & 0x02)
                        | ((c2 >> 1) & 0x01);
        }
}

extern  char    swplnsym[ORIENTS][ANGLES][SYMBYTES];
extern  char    swhitsym[HITSYMS][SYMBYTES];
extern  char    swbmbsym[BOMBANGS][BOMBBYTES];
extern  char    swtrgsym[TARGORIENTS][TARGBYTES];
extern  char    swwinsym[WINSIZES][WINBYTES];
extern  char    swhtrsym[TARGBYTES];
extern  char    swexpsym[EXPLSYMS][EXPBYTES];
extern  char    swflksym[FLCKSYMS][FLKBYTES];
extern  char    swbrdsym[BIRDSYMS][BRDBYTES];
extern  char    swoxsym[OXSYMS][OXBYTES];

static void
invert_symbols (void)
{
        invert ((char *) swplnsym, ORIENTS * ANGLES * SYMBYTES);
        invert ((char *) swhitsym, HITSYMS * SYMBYTES);
        invert ((char *) swbmbsym, BOMBANGS * BOMBBYTES);
        invert ((char *) swtrgsym, TARGORIENTS * TARGBYTES);
        invert ((char *) swwinsym, WINSIZES * WINBYTES);
        invert ((char *) swhtrsym, TARGBYTES);
        invert ((char *) swexpsym, EXPLSYMS * EXPBYTES);
        invert ((char *) swflksym, FLCKSYMS * FLKBYTES);
        copy ((char *) swbrdsym, (char *) spcbirds);
        invert ((char *) spcbirds, BIRDSYMS * BRDBYTES * 2);
        invert ((char *) swoxsym, OXSYMS * OXBYTES);
}
#endif





/*---------------------------------------------------------------------------

        External calls to specify current video ram as screen ram or
        auxiliary screen area.

---------------------------------------------------------------------------*/




void
use_on_screen_buffer (void)
{
#if 0
        static  char    *videoram = NULL;

        if ( !videoram )
                videoram = trap14( 3 );
        dispoff = videoram;
#endif
}



void
use_off_screen_buffer (void)
{
#if 0
        dispoff = auxdisp - 0x4000;
#endif
}


void
set_text_color (int x)
{
  /*--------------------------
;       swcolour( x )   -  Set the text display colour.

swcolour:
        push    BP                      ; save frame pointer
        mov     BP,SP                   ; access stack pointer
        mov     AL,4[BP]                ; save colour
        mov     textclr,AL              ; save colour
        pop     BP
        ret
  ---------------------------*/
}


void
position_text_cursor (int x, 
		      int y)
{
	/*------------------------
;       swposcur( x, y )   -  Position the character cursor to the
;       character position (x,y)

swposcur:
        push    BP                      ; save frame pointer
        mov     BP,SP                   ; access stack pointer

        mov     DH,6[BP]                ; y coordinate
        mov     DL,4[BP]                ; x coordinate
        xor     BH,BH                   ; page 0

        cmp     word ptr hires,1        ; hi resolution screen?
        jne     swpos1                  ;   NO - skip
        shl     DL,1                    ;   YES- double x coordinate
swpos1: mov     AH,02H                  ; position cursor
        int     10H                     ;

        pop     BP
        ret
	------------------------*/
}

