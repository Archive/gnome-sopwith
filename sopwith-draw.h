
#ifndef SOPWITH_DRAW_H
#define SOPWITH_DRAW_H


#include "sopwith-object.h"

void draw_map_object         (SopwithObject *obp);

void draw_player_plane       (SopwithObject *ob);
void draw_bomb               (SopwithObject *obp);
void draw_missile            (SopwithObject *obp);
void draw_starburst          (SopwithObject *obp);
void draw_explosion          (SopwithObject *obp);
void draw_enemy_plane        (SopwithObject *ob);
void draw_other_player_plane (SopwithObject *ob);
void draw_target             (SopwithObject *ob);
void draw_flock              (SopwithObject *ob);
void draw_bird               (SopwithObject *ob);
void draw_splatted_ox        (void);

#endif /* SOPWITH_DRAW_H */
