/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
        bmblib  -       SW Old BMB STDLIB routines

                        Copyright (C) 1984-2000 David L. Clark.

			Copyright (C) 2000 Maciej Stachowiak

                        All rights reserved except as specified in the
                        file license.txt.  Distribution of this file
                        without the license.txt file accompanying is
                        prohibited.

        Modification History:
                        94-12-19        Original development
		      2000-12-14        Started GNOME porting - see ChangeLog
                                        for further changes.
*/


#include "std.h"
#include <glib.h>
#include <string.h>


/*----------------------------------------------------------------------------
                GETFLAGS flags processing ( Jack Cole )
----------------------------------------------------------------------------*/


#if 0

int
getflags(int   *ac, 
	 char **av, 
	 char  *fmt, 
	 int   *flds[])
{
	char **arg, *apend;
	int i,j,k;
	char *aptr;
	char *flag;
	char *fptr;
	int  **var;
	int  *adrvar;
	char *cfmt;
	int  sts = 0;
	int  got_next;

	arg = (char **) *av;
	i = *ac;
	++arg;                              /* point past program name */
	
	while ( --i )  {                    /* for all args */
		aptr = *arg;                    /* point at string */
		if ( *aptr++ != '-' ) break;    /* past the switches */
		if ( *aptr == '-' ) {           /* or -- at eol */
			++arg;                  /* flush it */
			--i;
			break;
		}
		
	nextbool:
		flag = aptr;                            /* get the switch */
		var = (int **) &flds;                   /* find in format */
		
		for (fptr = fmt; *fptr && *fptr != ':' ; ++var) {
			j = 0;
			/* get switch length */
			while (isalnum (*(fptr+j))) {
				++j;           
			}

			/* match and number or space following? */
			
			if (!strncmp (flag, fptr, j) && (!isalpha (*(flag+j)) ||
							 *(fptr+j)!='#') ) {
				break;
			}

			fptr += j;                                  /* skip to next */
			if (*fptr) ++fptr;                          /* past format */
		}
		
		if (!(*fptr) || *fptr == ':')  {              /* no match? */
			if ((k = str_index (fmt, ':'))) {          /* find usage info */
				cfmt = fmt + k - 1;
				*cfmt++ = '\0';
				if (!(*cfmt)) {                       /* return on error? */
					sts = 1;
					goto ret;
				}
			}
			exit (0);
		}
		
		flag = fptr+j;                          /* the type */
		aptr += j;
		adrvar = *var;                          /* this is addr of real var */
		got_next = FALSE;
		if (*aptr == 0 && *flag != '&')  {      /* more expected */
			if (i > 1) {                      /* any more args? */
				aptr = *(++arg);                /* step to next arg */
				--i;
				got_next = TRUE;
			}
		}
		
		switch (*flag)  {                       /* what kind expected */
		case '#' :
			j = 0;
			if (*aptr) {                  /* any more chars? */
				*adrvar=strtol(aptr,&apend,10);
				j = apend - aptr;
				aptr = apend;
			}
			if (!j) {                     /* how many digits? */
				if (got_next) {         /* none - push back? */
					--arg;          /* yes, push back */
					++i;
				}
				*(int *)adrvar = -1;    /* flag present, but no arg */
			}
			break;
			
		case '*' : *(char **)adrvar = aptr;
                        break;
			
		case '&' : *(int *)adrvar = TRUE;              /* boolean */
                        break;
		}
		
		if (*flag == '&' && *aptr) {
			goto nextbool;
		}
		++arg;
	}
	
 ret:
	*av = (char *) arg;                          /* point past those processed */
	*ac = i;
	return sts;                                  /* successful */
}


int 
str_index (char *str,int c)
{
	char *s;
	s=strchr(str,c);
	
	return (s == NULL ? 0 : s-str+1);
}



void movmem(void *src,
	    void *dest,
	    unsigned count)
{
	memmove (dest, src, count);
}




void setmem (void         *dest,
	     unsigned int  count,
	     int           c)
{
	memset (dest, c, count);
}


void movblock(unsigned int srcoff,unsigned int srcseg,
              unsigned int destoff,unsigned int destseg,
              unsigned int count)
{
	movedata(srcseg,srcoff,destseg,destoff,count);
}



int inportb (unsigned port)
{
	return (inp (port));
}



int outportb (unsigned port,
	      int      data)
{
	return (outp (port,data));
}



int sysint(int intnum,struct regval *inrv,struct regval *outrv)
{
	union REGS regs;
	struct SREGS segregs;
	int rc;
	
	regs.x.ax=inrv->axr;
	regs.x.bx=inrv->bxr;
	regs.x.cx=inrv->cxr;
	regs.x.dx=inrv->dxr;
	segregs.ds=inrv->dsr;
	rc=int86x(intnum,&regs,&regs,&segregs);
	outrv->axr=regs.x.ax;
	outrv->bxr=regs.x.bx;
	outrv->cxr=regs.x.cx;
	outrv->dxr=regs.x.dx;
	outrv->dsr=segregs.ds;
	return(rc);
}


int sysint21(struct regval *inrv,struct regval *outrv)
{
	union REGS regs;
	struct SREGS segregs;
	int rc;
	
	regs.x.ax=inrv->axr;
	regs.x.bx=inrv->bxr;
	regs.x.cx=inrv->cxr;
	regs.x.dx=inrv->dxr;
	segregs.ds=inrv->dsr;
	rc=intdosx(&regs,&regs,&segregs);
	outrv->axr=regs.x.ax;
	outrv->bxr=regs.x.bx;
	outrv->cxr=regs.x.cx;
	outrv->dxr=regs.x.dx;
	outrv->dsr=segregs.ds;
	return(rc);
}

#endif
