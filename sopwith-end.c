/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
        swend    -      SW end of game

                        Copyright (C) 1984-2000 David L. Clark.

                        All rights reserved except as specified in the
                        file license.txt.  Distribution of this file
                        without the license.txt file accompanying is
                        prohibited.

                        Author: Dave Clark

        Modification History:
                        84-02-02        Development
                        87-03-09        Microsoft compiler.
                        87-03-12        Wounded airplanes.
		        2000-12-29      Started GNOME port, see ChangeLog for 
 		                          further changes.
*/

#include <config.h>
#include "sopwith-end.h"
#include "sopwith-graphics.h"
#include "sopwith-collision.h"

#include "sw.h"

#include <stdio.h>
#include <stdlib.h>



extern  int       playmode;             /* Mode of play ( SINGLE, MULTIPLE, */
                                        /*                or COMPUTER )     */
extern  int       savemode;             /* Saved PC video mode              */
extern  gboolean  hires;                /* High res debug/mono flag         */
extern  SopwithObject  *objtop;               /*  Start of object list.           */
extern  MULTIO   *multbuff;             /*  Communications buffer           */
extern  int       player;
extern  int       endsts[];             /* End of game status and move count*/
extern  int       endcount;
extern  gboolean  goingsun;             /* Heading for the sun flag         */
extern  SopwithObject  *objsmax;              /* Maximum object allocated         */
extern  gboolean  repflag;              /* Report statictics flag           */
extern  gboolean  inplay;               /*  Currently playing flag          */
extern  int       maxcrash;             /* Maximum number of crashes        */


static void report_game_statistics (void);


void
sopwith_end (char    *msg,
	     gboolean update)
{
	char *closmsg = NULL;

#if 0
        sound (0, 0, NULL);
        swsound();
#endif

        if (repflag) {
                report_game_statistics ();
	}
#if 0
        if (playmode == MULTIPLE) {
                closmsg = multclos (update);
        } else if (playmode == ASYNCH) {
                closmsg = asynclos ();
	}
#endif

#if 0
        intsoff();
        _intterm();
        intson();
        histend();
#endif

        puts ("\r\n");
        if (closmsg) {
                puts (closmsg);
                puts ("\r\n");
        }

        if (msg) {
                puts (msg);
                puts ("\r\n");
        }

        inplay = FALSE;

        if (msg || closmsg) {
                exit (1);
	} else {
                exit (0);
	}
}




void
end_game (int targclr)
{
	int winclr;
	SopwithObject *ob;

        if (((playmode != MULTIPLE) && (playmode != ASYNCH))
	    || (multbuff->mu_maxplyr == 1 )) {
		winclr = 1;
	} else if ((objtop + 1)->ob_score == objtop->ob_score) {
		winclr = 3 - targclr;
	} else {
		winclr = ((objtop + 1)->ob_score > objtop->ob_score) + 1;
	}

        ob = objtop;
        while (ob->ob_type == PLANE) {
                if (!endsts[ob->ob_index]) {
                        if ((ob->ob_clr == winclr)
			    && ((ob->ob_crashcnt < (MAXCRASH - 1))
				|| ((ob->ob_crashcnt < MAXCRASH)
				    && ((ob->ob_state == FLYING)
					|| (ob->ob_state == STALLED)
					|| (ob->ob_state == WOUNDED)
					|| (ob->ob_state == WOUNDSTALL))))) {
                                winner (ob);
                        } else {
                                loser (ob);
			}
		}
                ob = ob->ob_next;
        }
}


void
winner (SopwithObject *obp)
{
	int n;
	SopwithObject *ob;

        endsts[n = (ob = obp)->ob_index] = WINNER;
        
	if (n == player) {
                endcount = 72;
                goingsun = TRUE;
                ob->ob_dx = ob->ob_dy = ob->ob_ldx = ob->ob_ldy = 0;
                ob->ob_state = FLYING;
                ob->ob_life = MAXFUEL;
                ob->ob_speed = MIN_SPEED;
        }
}




void
loser (SopwithObject *ob)
{
	int n;
	
        endsts[n = ob->ob_index] = LOSER;
	
        if (n == player) {
                set_text_color (0x82);
                position_text_cursor (16, 12);
                puts ("THE END");
                endcount = 20;
        }
}



static void
report_game_statistics (void)
{
        puts ("\r\nEnd of game statictics\r\n\r\n");
        puts ("objects used: ");
        write_number (((long) objsmax - (long) objtop + 1) / sizeof (SopwithObject), 9);
        puts ("\r\n");
}
