/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/* 
 * Copyright (C) 2000 Eazel, Inc
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * As a special exception, you can distribute this file under the
 * terms of the Sopwith license instead.
 *
 * Author: Maciej Stachowiak <mjs@eazel.com> 
 */

/* sopwith-window.c
 */

#include <config.h>
#include "sopwith-window.h"

#include "sopwith-drawing-area.h"

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-i18n.h>
#include <libgnomeui/gnome-app-helper.h>
#include <libgnomeui/gnome-stock.h>
#include <libgnomeui/gnome-about.h>
#include <gtk/gtkmain.h>

struct SopwithWindowDetails {
	GtkWidget *drawing_area;
};

static void new_callback   (GtkWidget *widget, 
			    gpointer data);
static void exit_callback  (GtkWidget *widget, 
			    gpointer data);
static void about_callback (GtkWidget *widget, 
			    gpointer data);

static GnomeUIInfo game_menu[] = {
        GNOMEUIINFO_ITEM_STOCK(N_("New"), N_("Start a new game."),
                               new_callback, GNOME_STOCK_MENU_NEW),
	GNOMEUIINFO_MENU_EXIT_ITEM (exit_callback, NULL), 
        GNOMEUIINFO_END
};
static GnomeUIInfo help_menu[] = {
        GNOMEUIINFO_ITEM_STOCK(N_("About"), N_("About GNOME Sopwith."),
                               about_callback, GNOME_STOCK_MENU_ABOUT),
        
        GNOMEUIINFO_END
};

static GnomeUIInfo main_menu[] = {
        GNOMEUIINFO_MENU_GAME_TREE (game_menu),
        GNOMEUIINFO_MENU_HELP_TREE (help_menu),
        GNOMEUIINFO_END
};


static void sopwith_window_initialize_class       (SopwithWindowClass *klass);
static void sopwith_window_initialize             (SopwithWindow      *window);
static void sopwith_window_destroy                (GtkObject           *object);

static gpointer parent_class;                                                

GtkType
sopwith_window_get_type (void)
{
	GtkType parent_type;                                          
	static GtkType type;                                          
                                                                      
	if (type == 0) {                                              
		static GtkTypeInfo info = {                           
			"SopwithWindow",
			sizeof (SopwithWindow),
			sizeof (SopwithWindowClass),
			(GtkClassInitFunc)sopwith_window_initialize_class,
			(GtkObjectInitFunc)sopwith_window_initialize,
			NULL,
			NULL,
			NULL 
		};

		parent_type = GNOME_TYPE_APP;
		type = gtk_type_unique (parent_type, &info);
		parent_class = gtk_type_class (GNOME_TYPE_APP);
	}

	return type;
}

static void
sopwith_window_initialize_class (SopwithWindowClass *klass)
{
	GtkObjectClass *object_class;
	
	object_class = (GtkObjectClass *) klass;

	object_class->destroy = sopwith_window_destroy;
}

static void
sopwith_window_initialize (SopwithWindow *window)
{
	gnome_app_construct (GNOME_APP (window), "gnome-sopwith", "GNOME Sopwith");

	gtk_quit_add_destroy (1, GTK_OBJECT (window));
	
	window->details = g_new0 (SopwithWindowDetails, 1);
	window->details->drawing_area = gtk_widget_new (SOPWITH_TYPE_DRAWING_AREA, NULL);
	gtk_widget_show (window->details->drawing_area);

	gnome_app_set_contents (GNOME_APP (window),
				window->details->drawing_area);
	
	gnome_app_create_menus (GNOME_APP (window), main_menu);
}


static void 
sopwith_window_destroy (GtkObject *object)
{
	SopwithWindow *window;

	window = SOPWITH_WINDOW (object);
	g_free (window->details);

	(* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}


static void
exit_callback (GtkWidget *widget, 
	       gpointer data) 
{
	gtk_main_quit ();
}


static GtkWidget *about = NULL;

static void
delete_callback (GtkWidget *widget, gpointer data)
{
        about = NULL;
}

static void
about_callback (GtkWidget *widget, 
		gpointer data) 
{
	gchar *authors[] = {
                "David L. Clark <davidclark@home.com> (original DOS version)",
		"Maciej Stachowiak <mjs@eazel.com> (GNOME port)",
		NULL
	};

        if (about == NULL) {
                about = gnome_about_new(_("GNOME Sopwith"), VERSION,
					"Copyright (C) 1984-2000 David L. Clark,"
					"Copyright (C) 2000 Maciej Stachowiak",
                                        (const gchar **) authors,
                                        _("Side-scrolling aerial combat game."),
                                        NULL);
                gtk_signal_connect (GTK_OBJECT (about), "destroy", delete_callback, NULL);
        }
        gtk_widget_show (about);

}

static void
new_callback (GtkWidget *widget, 
		gpointer data) 
{

}


SopwithDrawingArea *
sopwith_window_get_drawing_area (SopwithWindow *window)
{
	return SOPWITH_DRAWING_AREA (window->details->drawing_area);
}


void
sopwith_window_show_title (SopwithWindow *window)
{
        show_title (window->details->drawing_area);
}
