/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

#ifndef SOPWITH_AUTO_PILOT_H
#define SOPWITH_AUTO_PILOT_H

#include "sopwith-object.h"

void auto_pilot (SopwithObject *ob);
void go_home    (SopwithObject *obpt);
void aim        (SopwithObject *obo, 
		 SopwithObject *obt,
		 int            ax, 
		 int            ay,
		 gboolean       longway);
int range       (int            x, 
		 int            y,
		 int            ax,
		 int            ay);

#endif /* SOPWITH_AUTO_PILOT_H */
