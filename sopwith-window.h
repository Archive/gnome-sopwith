/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/* 
 * Copyright (C) 2000 Eazel, Inc
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * As a special exception, you can distribute this file under the
 * terms of the Sopwith license instead.
 *
 * Author: Maciej Stachowiak <mjs@eazel.com> 
 */

/* sopwith-window.h
 */

#ifndef SOPWITH_WINDOW_H
#define SOPWITH_WINDOW_H

#include <libgnomeui/gnome-app.h>
#include "sopwith-drawing-area.h"

#define SOPWITH_TYPE_WINDOW	       (sopwith_window_get_type ())
#define SOPWITH_WINDOW(obj)	       (GTK_CHECK_CAST ((obj), SOPWITH_TYPE_WINDOW, SopwithWindow))
#define SOPWITH_WINDOW_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), SOPWITH_TYPE_WINDOW, SopwithWindowClass))
#define SOPWITH_IS_WINDOW(obj)	       (GTK_CHECK_TYPE ((obj), SOPWITH_TYPE_WINDOW))
#define SOPWITH_IS_WINDOW_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), SOPWITH_TYPE_WINDOW))

typedef struct SopwithWindowDetails SopwithWindowDetails;

typedef struct {
        GnomeApp base;
	SopwithWindowDetails *details;
} SopwithWindow;

typedef struct {
	GnomeAppClass base;
} SopwithWindowClass;

/* GtkObject support */
GtkType             sopwith_window_get_type         (void);


SopwithDrawingArea *sopwith_window_get_drawing_area (SopwithWindow *window);


void                sopwith_window_show_titles      (SopwithWindow *window);


#endif /* SOPWITH_WINDOW_H */

