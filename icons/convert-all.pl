#!/usr/bin/perl


@files = split (/ /, `echo *.xpm`);

for my $file (@files) {
    $file =~ s/\n//;
    
    my $file_mod = $file;
    
    $file_mod =~ s/xpm/png/;

    print "convert ${file} ${file_mod}\n";

    system ("convert ${file} ${file_mod}");

} 
