/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

#ifndef SOPWITH_GRAPHICS_H
#define SOPWITH_GRAPHICS_H

#include "sopwith-object.h"
#include "sopwith-drawing-area.h"


void draw_everything                 (void);
void draw_ground                     (SopwithDrawingArea* area);
void draw_sprite                     (int            x, 
				      int            y, 
				      SopwithObject *ob);
void draw_point                      (int            x, 
				      int            y, 
				      int            clr);
int  draw_point_checking_collisions  (int            x, 
				      int            y, 
				      int            color);
int  draw_sprite_checking_collisions (int            x, 
				      int            y,
				      SopwithObject *ob);

void set_text_color                  (int            x);
void position_text_cursor            (int            x, 
				      int            y);

void use_on_screen_buffer            (void);
void use_off_screen_buffer           (void);

void clear_collision_detection_area  (void);


#endif /* SOPWITH_GRAPHICS_H */
