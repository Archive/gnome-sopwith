/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
        swmain   -      SW mainline

                        Copyright (C) 1984-2000 David L. Clark.

                        All rights reserved except as specified in the
                        file license.txt.  Distribution of this file
                        without the license.txt file accompanying is
                        prohibited.

                        Author: Dave Clark

        Modification History:
                        84-02-02        Development
                        84-06-12        PC-jr Speed-up
                        85-10-31        Atari
                        87-03-09        Microsoft compiler.
                        87-03-12        Wounded airplanes.
                        87-04-06        Computer plane avoiding oxen.
                        96-12-26        Speed up game a bit
		        2000-12-25      Started GNOME port, see ChangeLog for 
 		                          further changes.
*/

#include <config.h>
#include "sopwith-init.h"

#include "sopwith-object.h"

#include "sopwith-window.h"


#include        <libgnome/gnome-defs.h>
#include        <libgnome/gnome-i18n.h>
#include        <libgnomeui/gnome-init.h>
#include        <gtk/gtkmain.h>
#include        <glib.h>
#include        <setjmp.h>

SopwithPlayMode  playmode;                /* Mode of play                     */

SopwithGameParameters *currgame;          /* Game parameters and current game */
SopwithObject *targets[MAX_TARG+MAX_OXEN];  /* Status of targets array          */
int       numtarg[2];                     /* Number of active targets by color*/
int       savemode;                       /* Saved PC display mode            */
int       tickmode;                       /* Tick action to be performed      */
int       counttick, countmove;           /* Performance counters             */
int       movetick, movemax;              /* Move timing                      */

int       gamenum;                        /* Current game number              */
int       gmaxspeed, gminspeed;           /* Speed range based on game number */
int       targrnge;                       /* Target range based on game number*/

MULTIO    multiost;                       /* Multiple player I/O buffer       */

int       multkey;                        /* Keystroke to be passed           */
MULTIO   *multbuff = &multiost;


int       multtick;                       /* Multiple user tick delay         */
gboolean  hires;                          /* High res flag                    */
gboolean  disppos;                        /* Display position flag            */
gboolean  titleflg;                       /* Title flag                       */
int       dispdbg;                        /* Debug value to display           */
gboolean  soundflg;                       /* Sound flag                       */
gboolean  repflag;                        /* Report statistics flag           */
gboolean  joystick;                       /* Joystick being used              */
gboolean  ibmkeybd;                       /* IBM-like keyboard being used     */
gboolean  inplay;                         /* Game is in play                  */
gboolean  printflg = 0;                   /* Print screen requested           */
int       koveride;                       /* Keyboard override index number   */
int       missok;                         /* Missiles supported               */

int       displx, disprx;                 /* Display left and right           */
int       dispdx;                         /* Display shift                    */
gboolean  dispinit;                       /* Inialized display flag           */

SopwithObject  *drawlist;                       /* Onscreen object list             */
SopwithObject  *nobjects;                       /* SopwithObject list.                    */
SopwithObject   oobjects;                       /* Original plane object description*/
SopwithObject  *objbot, *objtop,                /* Top and bottom of object list    */
               *objfree,                        /* Free list                        */
               *deltop, *delbot;                /* Newly deallocated SopwithObject        */
SopwithObject   topobj, botobj;                 /* Top and Bottom of obj. x list    */

SopwithObject  *compnear[MAX_PLYR];             /* Planes near computer planes      */
int       lcompter[MAX_PLYR] = {          /* Computer plane territory         */
          0, 1155, 0,    2089
};
int       rcompter[MAX_PLYR] = {          /* Computer plane territory         */
          0, 2088, 1154, 10000
};

SopwithObject  *objsmax        =       0;       /* Maximum object allocated         */
int       endsts[MAX_PLYR];               /* End of game status and move count*/
int       endcount;
int       player;                         /* Pointer to player's object       */
int       currobx;                        /* Current object index             */
gboolean  plyrplane;                      /* Current object is player flag    */
gboolean  compplane;                      /* Current object is a comp plane   */
OLDWDISP  wdisp[MAX_OBJS];                /* World display status             */
gboolean  goingsun;                       /* Going to the sun flag            */
gboolean  forcdisp;                       /* Force display of ground          */
char     *histin, *histout;               /* History input and output files   */
unsigned  explseed;                       /* random seed for explosion        */

int       keydelay = -1;                  /* Number of displays per keystroke */
int       dispcnt;                        /* Displays to delay keyboard       */
int       endstat;                        /* End of game status for curr. move*/
int       maxcrash;                       /* Maximum number of crashes        */
int       shothole;                       /* Number of shot holes to display  */
int       splatbird;                      /* Number of slatted bird symbols   */
int       splatox;                        /* Display splatted ox              */
int       oxsplatted;                     /* An ox has been splatted          */


int       sintab[ANGLES] = {              /* sine table of pi/8 increments    */
          0,      98,     181,    237,    /*   multiplied by 256              */
          256,    237,    181,    98,
          0,      -98,    -181,   -237,
          -256,   -237,   -181,   -98
          };

jmp_buf   envrestart;                     /* Restart environment for restart  */
                                          /*  long jump.                      */




int
main (int    argc, 
      char **argv)
{
	GtkWidget *window;

	gnome_init ("gnome-sopwith", VERSION,
		    argc, argv);

	/* Suck up to prima donna libraries */
	gdk_rgb_init ();
	gtk_widget_push_colormap (gdk_rgb_get_cmap ());
	gtk_widget_push_visual (gdk_rgb_get_visual ());


#if 0 /* FIXME: Replace with something popt-based */	
        if ( getflags( &argc, &argv,
		       /*---- 96/12/27------
			 "n&s&c&m&a&k&i&j&q&h*v*r&d*f*n*t#w&y#e&g#x&:",
			 &n, &s, &c, &m, &a, &k, &ibmkeybd, &joystick, &soundflg,
			 &histout, &histin, &reset, &device,
			 &multfile, &cmndfile, &multtick, &hires, &keydelay,
			 &repflag, &gamenum, &missok )
			 ------ 96/12/27----*/
		       /*---- 99/01/24------
			 "n&s&c&m&a&k&j&q&h*v*r&d*f*t#w&y#e&g#x&:",
			 &n, &s, &c, &m, &a, &k, &joystick, &soundflg,
			 &histout, &histin, &reset, &device,
			 &multfile, &multtick, &hires, &keydelay,
			 &repflag, &gamenum, &missok )
			 ------ 99/01/24----*/
		       "n&s&c&a&k&j&q&x&:",
		       &n, &s, &c, &a, &k, &joystick, &soundflg,
		       &missok )
	     || ( ( modeset = n + s + c + m + a ) > 1 )
	     || ( ( keyset = joystick + k ) > 1 ) ) {
                disphelp( helptxt );
                exit( 1 );
        }
#endif


	window = gtk_widget_new (SOPWITH_TYPE_WINDOW, NULL);
	gtk_widget_show (window);

	sopwith_init ();

	sopwith_window_show_title (SOPWITH_WINDOW (window));

	gtk_main ();

	return 0;
}




