/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/* 
 * Copyright (C) 2000 Eazel, Inc
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * As a special exception, you can distribute this file under the
 * terms of the Sopwith license instead.
 *
 * Author: Maciej Stachowiak <mjs@eazel.com> 
 */

/* sopwith-drawing-area.c
 */

#include <config.h>
#include "sopwith-drawing-area.h"

#include <gtk/gtksignal.h>

#include <stdio.h>

#define WIDTH  640
#define HEIGHT 400
#define SCALE  2

struct SopwithDrawingAreaDetails {
	GdkGC *black_gc;
	GdkGC *magenta_gc;
	GdkGC *cyan_gc;
	GdkGC *white_gc;

	GdkPixmap *on_screen;
	GdkPixmap *off_screen;
};

static void sopwith_drawing_area_realize          (SopwithDrawingArea      *area,
						   gpointer                 data);

static void sopwith_drawing_area_initialize_class (SopwithDrawingAreaClass *klass);
static void sopwith_drawing_area_initialize       (SopwithDrawingArea      *area);
static void sopwith_drawing_area_destroy          (GtkObject               *object);

static gpointer parent_class;                                                

GtkType
sopwith_drawing_area_get_type (void)
{
	GtkType parent_type;                                          
	static GtkType type;                                          
                                                                      
	if (type == 0) {                                              
		static GtkTypeInfo info = {                           
			"SopwithDrawingArea",
			sizeof (SopwithDrawingArea),
			sizeof (SopwithDrawingAreaClass),
			(GtkClassInitFunc)sopwith_drawing_area_initialize_class,
			(GtkObjectInitFunc)sopwith_drawing_area_initialize,
			NULL,
			NULL,
			NULL 
		};

		parent_type = GTK_TYPE_DRAWING_AREA;
		type = gtk_type_unique (parent_type, &info);
		parent_class = gtk_type_class (GTK_TYPE_DRAWING_AREA);
	}
	
	return type;
}

static void
sopwith_drawing_area_initialize_class (SopwithDrawingAreaClass *klass)
{
	GtkObjectClass *object_class;
	
	object_class = (GtkObjectClass *) klass;

	object_class->destroy = sopwith_drawing_area_destroy;
}

static void
sopwith_drawing_area_initialize (SopwithDrawingArea *area)
{
	gtk_drawing_area_size (GTK_DRAWING_AREA (area),
			       WIDTH, HEIGHT);

	area->details = g_new0 (SopwithDrawingAreaDetails, 1);

	gtk_signal_connect (GTK_OBJECT (area), "realize", 
			    sopwith_drawing_area_realize, NULL);
}


static void 
sopwith_drawing_area_destroy (GtkObject *object)
{
	SopwithDrawingArea *area;

	area = SOPWITH_DRAWING_AREA (object);
	g_free (area->details);

	(* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}


static void
sopwith_drawing_area_realize (SopwithDrawingArea *area,
			      gpointer            data)
{
	area->details->on_screen = gdk_pixmap_new (GTK_WIDGET (area)->window,
						   WIDTH, HEIGHT, -1);

	area->details->black_gc = gdk_gc_new (GTK_WIDGET (area)->window);
	gdk_rgb_gc_set_foreground (area->details->black_gc, 0);
	gdk_rgb_gc_set_background (area->details->black_gc, 0);

	area->details->magenta_gc = gdk_gc_new (GTK_WIDGET (area)->window);
	gdk_rgb_gc_set_foreground (area->details->magenta_gc, 0xff00ff);
	gdk_rgb_gc_set_background (area->details->magenta_gc, 0);

	area->details->cyan_gc = gdk_gc_new (GTK_WIDGET (area)->window);
	gdk_rgb_gc_set_foreground (area->details->cyan_gc, 0x00ffff);
	gdk_rgb_gc_set_background (area->details->cyan_gc, 0);

	area->details->white_gc = gdk_gc_new (GTK_WIDGET (area)->window);
	gdk_rgb_gc_set_foreground (area->details->white_gc, 0xffffff);
	gdk_rgb_gc_set_background (area->details->white_gc, 0);
	

	gdk_draw_rectangle (area->details->on_screen, 
			    area->details->black_gc,
			    TRUE, 0, 0, WIDTH, HEIGHT);

	gdk_window_set_back_pixmap (GTK_WIDGET (area)->window,
				    area->details->on_screen,
				    FALSE);

	gdk_window_clear (GTK_WIDGET (area)->window);
}

static int
translate_x (int x)
{
	return SCALE * x;
}

static int
translate_y (int y)
{
	return HEIGHT -  (SCALE * y);
}


void
sopwith_drawing_area_draw_sprite (SopwithDrawingArea *area,
				  int                 x,
				  int                 y,
				  GdkPixmap          *pixmap,
				  GdkBitmap          *mask)
{
	GdkRectangle rect;

	gdk_gc_set_clip_origin (area->details->black_gc,
				translate_x (x),
				translate_y (y));

	gdk_gc_set_clip_mask (area->details->black_gc,
			      mask);

	gdk_gc_set_function (area->details->black_gc,
			     GDK_XOR);


	gdk_draw_pixmap	 (area->details->on_screen,
			  area->details->black_gc,
			  pixmap,
			  0, 0,
			  translate_x (x), translate_y (y),
			  -1, -1);

	/* FIXME: clear_area instead */
		
	gdk_window_clear (GTK_WIDGET (area)->window);

	gdk_gc_set_function (area->details->black_gc,
			     GDK_COPY);

	rect.x = 0;
	rect.y = 0;
	rect.width = WIDTH;
	rect.height = HEIGHT;
	
	gdk_gc_set_clip_rectangle (area->details->black_gc,
				   &rect);
}




void
sopwith_drawing_area_draw_point (SopwithDrawingArea *area,
				 int                 x,
				 int                 y,
				 int                 color)
{
	GdkGC *gc;

	switch (color) {
	case 0:
		gc = area->details->black_gc;
		break;
	case 1:
		gc = area->details->cyan_gc;
		break;
	case 2:
		gc = area->details->magenta_gc;
		break;
	case 3:
		gc = area->details->white_gc;
		break;
	}

	gdk_gc_set_function (gc, GDK_XOR);

	gdk_draw_rectangle (area->details->on_screen, 
			    gc,
			    TRUE, translate_x (x), translate_y (y),
			    2, 2);


	gdk_window_clear_area (GTK_WIDGET (area)->window,
			       translate_x (x), translate_y (y),
			       2, 2);

	gdk_gc_set_function (gc, GDK_COPY);
}
