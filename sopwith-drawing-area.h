/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/* 
 * Copyright (C) 2000 Eazel, Inc
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * As a special exception, you can distribute this file under the
 * terms of the Sopwith license instead.
 *
 * Author: Maciej Stachowiak <mjs@eazel.com> 
 */

/* sopwith-drawing-area.h
 */

#ifndef SOPWITH_DRAWING_AREA_H
#define SOPWITH_DRAWING_AREA_H

#include <gtk/gtkdrawingarea.h>

#define SOPWITH_TYPE_DRAWING_AREA	     (sopwith_drawing_area_get_type ())
#define SOPWITH_DRAWING_AREA(obj)	     (GTK_CHECK_CAST ((obj), SOPWITH_TYPE_DRAWING_AREA, SopwithDrawingArea))
#define SOPWITH_DRAWING_AREA_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), SOPWITH_TYPE_DRAWING_AREA, SopwithDrawingAreaClass))
#define SOPWITH_IS_DRAWING_AREA(obj)	     (GTK_CHECK_TYPE ((obj), SOPWITH_TYPE_DRAWING_AREA))
#define SOPWITH_IS_DRAWING_AREA_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), SOPWITH_TYPE_DRAWING_AREA))

typedef struct SopwithDrawingAreaDetails SopwithDrawingAreaDetails;

typedef struct {
	GtkDrawingArea base;
	SopwithDrawingAreaDetails *details;
} SopwithDrawingArea;

typedef struct {
	GtkDrawingAreaClass base;
} SopwithDrawingAreaClass;

/* GtkObject support */
GtkType sopwith_drawing_area_get_type      (void);

void    sopwith_drawing_area_draw_sprite   (SopwithDrawingArea *area,
					    int                 x,
					    int                 y,
					    GdkPixmap          *pixmap,
					    GdkBitmap          *mask);

void    sopwith_drawing_area_draw_point    (SopwithDrawingArea *area,
					    int                 x,
					    int                 y,
					    int                 color);

#endif /* SOPWITH_DRAWING_AREA_H */

