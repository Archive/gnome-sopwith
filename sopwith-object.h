
#ifndef SOPWITH_OBJECT_H
#define SOPWITH_OBJECT_H

#include "sw.h"

typedef enum {
	WAITING      = 0,
	FLYING       = 1,
	HIT          = 2,
	CRASHED      = 4,
	FALLING      = 5,
	STANDING     = 6,
	STALLED      = 7,
	REBUILDING   = 8,
	WOUNDED      = 9,
	WOUNDSTALL   = 10,
	FINISHED     = 91,
	GHOST        = 92,
	GHOSTCRASHED = 93,
	GHOSTSTALLED = 94
} SopwithObjectState;

typedef enum { /*  Object types                   */
	GROUND = 0,
	PLANE = 1,
	BOMB = 2,
	SHOT = 3,
	TARGET = 4,
	EXPLOSION = 5,
	SMOKE = 6,
	FLOCK = 7,
	BIRD = 8,
	OX = 9,
	MISSILE = 10,
	STARBURST = 11,
	DUMMYTYPE = 99
} SopwithObjectType;

typedef struct SopwithObject SopwithObject;

struct SopwithObject {
        SopwithObjectState ob_state;
        int                ob_x, ob_y;
        int                ob_dx, ob_dy;
        int                ob_angle;
        int                ob_orient;
        int                ob_speed;
        int                ob_accel;
        int                ob_flaps;
        SopwithObject     *ob_firing;
        int                ob_score;
        int                ob_rounds;
        int                ob_hitcount;
        int                ob_updcount;
        int                ob_life;
        SopwithObject     *ob_owner;
        int                ob_symhgt;
        int                ob_symwdt;
        int                ob_bombing;
        int                ob_bombs;
        int                ob_clr;
        int                ob_lx;
        int                ob_ly;
        int                ob_ldx;
        int                ob_ldy;
        SopwithObject     *ob_next;
        SopwithObject     *ob_prev;
        int                ob_index;
        int                ob_oldx;
        int                ob_oldy;
        int                ob_drwflg;
        int                ob_delflg;
        char              *ob_oldsym;
        void             (*ob_drawf) (SopwithObject *);
        int              (*ob_movef) (SopwithObject *);
        SopwithObject     *ob_xnext;
        SopwithObject     *ob_xprev;
        int                ob_crashcnt;
        char              *ob_newsym;
        int                ob_bdelay;
        int                ob_home;
        int                ob_hx[3];
        int                ob_hy[3];
        SopwithObjectType  ob_type;
        SopwithObject     *ob_dnext;
        int                ob_athome;
        struct tt         *ob_sound;
        int                ob_missiles;
        SopwithObject     *ob_mfiring;
        int                ob_mdelay;
        SopwithObject     *ob_target;
        int                ob_bursts;
        int                ob_bfiring;
        int                ob_bsdelay;
};


void           initialize_objects (void);
SopwithObject *allocate_object    (void);
void           free_object        (SopwithObject *obp);

#endif SOPWITH_OBJECT_H
