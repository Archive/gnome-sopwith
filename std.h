/*

                        Copyright (C) 1984-2000 David L. Clark.

                        All rights reserved except as specified in the
                        file license.txt.  Distribution of this file
                        without the license.txt file accompanying is
                        prohibited.

			2000-12-14    Started GNOME porting. See
			                ChangeLog for further changes.
*/

#ifndef STD_H
#define STD_H

int str_index (char *str,
	       int c);
int inportb   (unsigned int port);

void movblock (unsigned int srcoff,unsigned int srcseg,
	       unsigned int destoff,unsigned int destseg,
	       unsigned int count);
void movmem(void *src,void *dest,unsigned count);
int outportb(unsigned port,int data);
void setmem(void *dest,unsigned count,int c);


#endif /* STD_H */

