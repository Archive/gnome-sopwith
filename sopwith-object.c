/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
        swobject -      SW object allocation and deallocation

                        Copyright (C) 1984-2000 David L. Clark.

                        All rights reserved except as specified in the
                        file license.txt.  Distribution of this file
                        without the license.txt file accompanying is
                        prohibited.

                        Author: Dave Clark

        Modification History:
                        84-02-07        Development
                        84-06-12        PCjr Speed-up
                        84-10-31        Atari
                        87-03-09        Microsoft compiler.
		        2000-12-26      Started GNOME port. See ChangeLog 
			                  for further changes.
*/

#include        "sopwith-object.h"
#include        "sopwith-globals.h"


extern  SopwithObject *nobjects;            /* SopwithObjects list.                    */
extern  SopwithObject *objbot, *objtop,     /* Top and bottom of object list    */
	               *objfree,             /* Free list                        */
                       *deltop, *delbot;     /* Newly deallocated SopwithObjects        */
extern  SopwithObject *objsmax;             /* Maximum allocated object         */



void
initialize_objects (void)
{
	SopwithObject *ob;
	int o;

        topobj.ob_xnext = topobj.ob_next = &botobj;
        botobj.ob_xprev = botobj.ob_prev = &topobj;
        topobj.ob_x = -32767;
        botobj.ob_x = 32767;

        objbot = objtop = deltop = delbot = NULL;
        objfree = ob = nobjects;

        for (o = 0; o < MAX_OBJS; ++o) {
                ob->ob_next = ob + 1;
                (ob++)->ob_index = o;
        }

        (--ob)->ob_next = NULL;
}



SopwithObject *
allocate_object (void)
{
	SopwithObject *ob;

        if (!objfree) {
                return (NULL);
	}

        ob = objfree;
        objfree = ob->ob_next;

        ob->ob_next = NULL;
        ob->ob_prev = objbot;

        if (objbot) {
                objbot->ob_next = ob;
	} else {
                objtop = ob;
	}

        ob->ob_sound = NULL;
        ob->ob_drwflg = ob->ob_delflg = 0;
        if (ob > objsmax) {
                objsmax = ob;
	}

	objbot = ob;

        return ob;
}



void
free_object (SopwithObject *obp)
{
	SopwithObject *ob, *obb;

        ob = obp;
	obb = ob->ob_prev;
        if (obb) {
                obb->ob_next = ob->ob_next;
        } else {
                objtop = ob->ob_next;
	}

	obb = ob->ob_next;
        if (obb) {
                obb->ob_prev = ob->ob_prev;
        } else {
                objbot = ob->ob_prev;
	}

        ob->ob_next = 0;
        if (delbot) {
                delbot->ob_next = ob;
        } else {
                deltop = ob;
	}

        delbot = ob;
}

